#!/usr/bin/env python

from nose import with_setup


def init_func():
    print("j'initialise !!")


def end_func():
    print("je finalise !!")


@with_setup(init_func, end_func)
def test1():
    print("test 1")
