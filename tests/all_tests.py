#!/usr/bin/env python

import unittest


def allTests():
    from test_assert import TestAssert
    from test_fibonacci import TestFibo

    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestAssert))
    suite.addTest(unittest.makeSuite(TestFibo))

    return suite


if __name__ == '__main__':
    unittest.TextTestRunner(verbosity=2).run(allTests())
