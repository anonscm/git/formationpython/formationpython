The example module
======================

Using factorial
-------------------

This is an example text file in reStructuredText format.  First import
factorial from the example module:

    >>> from fibonacci import factorielle

Now use it:

    >>> factorielle(5)
    120
