#!/usr/bin/env python

import unittest


class TestAssert(unittest.TestCase):

    def test_equal(self):
        self.assertEqual(2, 1 + 1)

    def test_false(self):
        self.assertFalse(1 == 1 + 1)

    def test_almostEqual(self):
        self.assertAlmostEqual(0.000011, 0.000012, places=5)


if __name__ == '__main__':
    unittest.main(verbosity=2)  # par defaut verbosity=1
