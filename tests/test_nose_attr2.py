#!/usr/bin/env python

from nose.plugins.attrib import attr


@attr(vitesse='lent')
def test_lent():
    print('test lent')


@attr(vitesse='rapide')
def test_rapide():
    print('test rapide')
