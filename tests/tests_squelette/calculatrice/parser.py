import re


def parser(s):
    """
    décompose une expression en une liste de nombres
    et une liste d'opérateurs.

    :param s: expression à décomposer

    :return numList: liste des nombres constituant l'expression
    :return opList: liste des opérateurs constituant l'expression

    """
    parse_nombres= re.compile(r'([.]?\d+\.?\d*)')
    parse_operateurs = re.compile(r'[-+*/]')

    nombres = list(map(float, parse_nombres.findall(s)))
    operateurs = parse_operateurs.findall(s)

    if len(operateurs) != len(nombres):
        operateurs.insert(0, '+')

    return nombres, operateurs
