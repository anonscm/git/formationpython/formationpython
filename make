#!/bin/bash

function my_help {
    cat << 'EOF'

Usage: ./make [a_command]

Liste des commandes disponibles:

    jour1_matin     : Introduction, présentation du langage
    jour2_matin     : Modules et distribution de paquets  
    jour2_apresmidi : Programmation objet

    html            : création de la sortie HTML
    pdf             : création de la sortie PDF
    sh              : shell ipython
    update          : install/update le virtualenv

EOF
    exit 1
}

function enter_virtualenv {
    if [ ! -d env ]
    then
        update
    fi
    source env/bin/activate 2>/dev/null
    if [ ! $? -eq 0 ]
    then
        echo
        echo "Virtualenv does not work !"
        echo
        exit 2
    fi
}

function update {
    if [ ! -d env ]
    then
        # bug dans le pyvenv-3.4 de certains distributions
        PYENV="/usr/bin/pyvenv"
        if [ ! -e $PYENV ]
        then
            echo "[$PYENV] absent !"
            exit 5
        fi
        $PYENV --without-pip env
        source env/bin/activate
        curl https://bootstrap.pypa.io/get-pip.py | python
        deactivate
    fi
    enter_virtualenv
    echo "Mise a jour des 'requirements'"
    if [ "$(pip --version|grep -c 'pip 1.4')" == "1" ]
    then
        # option inexistante en version 1.4 de pip
        ALLOW=""
    else
        ALLOW="--allow-external PIL --allow-unverified PIL"
    fi
    pip install --upgrade --requirement requirements.txt $ALLOW
    if [ $? != 0 ]
    then
        echo "!!! pip n'a pu installé tous les paquets !!!"
        exit
    fi
}

if [ ! $# -eq 1 ]
then
    my_help
fi

case "$1" in
update)
    update
    ;;
sh)
    enter_virtualenv
    ipython
    ;;
jour1_matin)
    enter_virtualenv
    ipython notebook notebooks/Introduction.ipynb
    ;;
jour1_apresmidi)
    enter_virtualenv
    # ipython notebook notebooks/Fonctions.ipynb
    make html
    echo
    echo "fichier HTML ici: build/html/fonctions.html"
    echo "fichier source ici: source/fonctions.rst"
    ;;
jour2_matin)
    enter_virtualenv
    ipython notebook notebooks/Modules.ipynb
    ;;
jour2_apresmidi)
    enter_virtualenv
    ipython notebook notebooks/Objets.ipynb
    ;;
jour3_matin)
    enter_virtualenv
    # ipython notebook notebooks/Tests.ipynb
    make html
    echo
    echo "fichier HTML ici: build/html/tests.html"
    echo "fichier source ici: source/tests.rst"
    ;;
jour3_apresmidi)
    enter_virtualenv
    # ipython notebook notebooks/Projet.ipynb
    make html
    echo
    echo "fichier HTML ici: build/html/projet.html"
    echo "fichier source ici: source/projet.rst"
    ;;
html)
    enter_virtualenv
    make html
    echo "fichier HTML ici: build/html/index.html"
    ;;
pdf)
    enter_virtualenv
    make latexpdf
    echo "fichier PDF ici: build/latex/FormationPython3.pdf"
    ;;
*)
    my_help
    ;;
esac
