#!/usr/bin/env python

import csv
import ballPack


def save_balls(balls, filename, delimiter=','):
    """
    sauvegarde d'une liste de balles au format csv.

    :param balls: liste à sauvegarder
    :param filename: fichier de la sauvegarde
    :param delimiter: délimiteur entre chaque entrées (défaut: ',')
    """

    with open(filename, 'w') as csvfile:
        ballwriter = csv.writer(csvfile, delimiter=delimiter,
                                quotechar='\\', quoting=csv.QUOTE_MINIMAL)
        for b in balls:
            ballwriter.writerow([b.__class__, b.coords[0], b.coords[1], b.v[0], b.v[
                                1], b.radius, b.domain[0], b.domain[1], '"' + b.color + '"'])


def read_balls(filename, delimiter=','):
    """
    lit un fichier csv contenant une liste de balles et
    renvoie cette liste en les contruisant.

    :param filename: fichier de la sauvegarde
    :param delimiter: délimiteur entre chaque entrées (défaut: ',')
    :return: liste de balles
    """
    balls = []
    with open(filename, 'r') as csvfile:
        ballreader = csv.reader(csvfile, delimiter=delimiter,
                                quotechar='\\', quoting=csv.QUOTE_MINIMAL)
        for row in ballreader:
            exec("b=" + row[0] + '(' + ','.join(row[1:]) + ')')
            balls.append(b)
    return balls

if __name__ == '__main__':
    import tempfile
    filename = tempfile.NamedTemporaryFile()

    bin = [ball.BouncingBall(0, 1), ball.GravityBall(0, 1)]
    save_balls(bin, filename.name)
    bout = read_balls(filename.name)

    print(bout)
