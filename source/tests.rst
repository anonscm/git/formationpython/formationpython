=========
Les tests
=========

Pourquoi écrire des tests ?
===========================

Lorsque l'on écrit un programme, il est généralement constitué de
plusieurs fonctions que l'on assemble afin de décrire notre algorithme
permettant de nous donner la réponse à notre problème. Un programme
n'est pas forcément un développement sur un temps court.

On voit beaucoup de bibliothèques scientifiques qui ont plus de dix ans.
Les fonctions peuvent donc être écrites à différents moments avec des
échelles de temps bien différentes. On peut par exemple ajouter une
fonctionnalité à un bout de code plusieurs années après en avoir écrit
le coeur.

Si il est primordial d'écrire de la documentation pour comprendre ce qui
est fait, il est également judicieux d'écrire des tests pour s'assurer
du bon fonctionnement de notre programme.

Il faut noter que certains types de développement logiciel s'appuient
sur les tests (`Test Driven
Development <http://fr.wikipedia.org/wiki/Test_Driven_Development>`__).

Les types de tests
==================

On peut citer trois types de tests primordiaux permettant de s'assurer
au mieux de l'absence de bugs dans notre programme. Un programme n'est
jamais à 100% sûr.

-  les **tests unitaires**,

-  les **tests d'intégration**,

-  les **tests du système complet**.

Tests unitaires: niveau 0
-------------------------

Le but est de tester chaque petit bout de code: fonctions, méthodes, ...

Ils permettent d'être sûr que chaque brique de votre programme
fonctionne correctement indépendamment des autres.

Néanmoins, ils ne permettent pas d'assurer le bon fonctionnement du
programme dans sa globalité.

Tests d'intégration: niveau 1
-----------------------------

Le but est de commencer à tester de petites interactions entre les
différentes unités du programme.

Ces tests peuvent être réalisés avec les mêmes outils que ceux utilisés
dans les tests unitaires.

Mais il y a une différence importante: on suppose que les unités prises
une à une sont valides.

Tests du système complet: niveau 2
----------------------------------

Le but est de tester le programme dans sa globalité.

On assemble à présent toutes les briques pour un problème concret.

Là encore, si les 2 premiers niveaux sont négligés les tests du système
complet ne servent à rien.

Les tests sont donc écrits à des stades différents du développement mais
ont chacun leur importance. Un seul de ces trois types de tests ne
suffit pas pour tester l'intégrité du programme.

Les tests unitaires et les tests d'intégration sont généralement testés
avec les mêmes outils.

Pour le dernier type de tests, on prendra des exemples concrets
d'exécution et on testera la sortie avec une solution certifiée.

Notre cas d'étude
=================

Nous allons calculer les coefficients de la suite de Fibonacci en
utilisant les coefficients binomiaux. Les coefficients binomiaux se
calculent à partir de la formule suivante

.. math::

   \left(
   \begin{array}{c}
   n \\
   k
   \end{array}
   \right)=C_n^k=\frac{n!}{k!(n-k)!} \; \text{pour} \; k=0,\cdots,n.

On en déduit alors le calcul des coefficients de la suite de Fibonacci
par la formule suivante

.. math::

   \sum_{k=0}^n
   \left(
   \begin{array}{c}
   n-k \\
   k
   \end{array}
   \right)
   = F(n+1).

Voici un exemple de code Python implantant cette formule (fibonacci.py)

.. literalinclude:: ../tests/fibonacci.py
    :linenos:


On souhaite faire les tests suivants

-  **tests unitaires**: tester si les fonctions *factorielle* et *somme*
   fonctionnent correctement.
-  **tests d'intégration**: tester si les fonctions *factorielle* et
   *somme* fonctionnent correctement ensemble, tester si la fonction
   *coef\_binomial* fonctionne correctement.
-  **tests du système complet**: tester si la fonction *fibonacci* donne
   le bon résultat.

Les outils de tests en Python
=============================

Il existe différents outils en Python permettant de réaliser des tests
(https://wiki.python.org/moin/PythonTestingToolsTaxonomy). Nous nous
intéresserons ici à trois d'entre eux

-  doctest
-  unittest
-  nosetests

doctest
=======

**doctest** permet de faire des tests basiques en s'appuyant par exemple
sur les docstrings.

**Rappel**: les docstrings permettent d'écrire de la documentation très
facilement directement avec le code source. Elles se placent
tout de suite après une méthode, une fonction, une classe. On rappelle
ici brièvement le principe en écrivant une documentation pour une
fonction.

.. code:: python

    def add(a, b):
        """Addition de 2 nombres a et b
        """
        return a + b


.. code:: python

    help(add)


.. parsed-literal::

    Help on function add in module __main__:

    add(a, b)
        Addition de 2 nombres a et b


**doctest** effectue

-  une recherche dans les sources des bouts de texte qui ressemblent à
   une session interactive Python,
-  une recherche dans des fichiers textes des bouts de texte qui
   ressemblent à une session interactive Python,
-  une exécution de ces bouts de session pour voir si le résultat est
   conforme.

Les sessions interactives sont représentées par le symbole **>>>**.

Pour l'utiliser, il suffit d'importer le module **doctest** et d'appeler
**testmod** si on veut tester l'ensemble d'un module comme dans notre
exemple **fibonacci.py**.

Voici un exemple de la sortie:

.. parsed-literal::

    python fibonacci.py


.. parsed-literal::

    Trying:
        coef_binomial(4, 2)
    Expecting:
        6
    **********************************************************************
    File "fibonacci.py", line 40, in __main__.coef_binomial
    Failed example:
        coef_binomial(4, 2)
    Expected:
        6
    Got:
        6.0
    Trying:
        factorielle(0)
    Expecting:
        1
    ok
    Trying:
        factorielle(5)
    Expecting:
        120
    ok
    Trying:
        fibonacci(10)
    Expecting:
        [1, 1, 2, 3, 5, 8, 13, 21, 34, 55]
    ok
    Trying:
        somme(0, 10, lambda k:k)
    Expecting:
        55.0
    ok
    Trying:
        somme(1, 8, lambda k: 2**k)
    Expecting:
        510.0
    ok
    1 items had no tests:
        __main__
    3 items passed all tests:
       2 tests in __main__.factorielle
       1 tests in __main__.fibonacci
       2 tests in __main__.somme
    **********************************************************************
    1 items had failures:
       1 of   1 in __main__.coef_binomial
    6 tests in 5 items.
    5 passed and 1 failed.
    ***Test Failed*** 1 failures.


Il est également possible d'écrire les tests dans un fichier texte, par exemple
au format RST (fibonacci.rst):

.. literalinclude:: ../tests/fibonacci.rst
    :linenos:


.. parsed-literal::

  python -m doctest -v fibonacci.rst


.. parsed-literal::

    Trying:
    from fibonacci import factorielle
        Expecting nothing
    ok
    Trying:
        factorielle(5)
    Expecting:
        120
    ok
    1 items passed all tests:
       2 tests in fibonacci.rst
    2 tests in 1 items.
    2 passed and 0 failed.
    Test passed.


Si **doctest** est très simple d'utilisation, on se rend bien compte
qu'il est assez limité et qu'il ne permet pas de faire des tests très
élaborés.

Plus de documentation ici:
`doctest <https://docs.python.org/3.4/library/doctest.html>`_

unittest
========

Ce module est également appelé PyUnit et reprend l'esprit de JUnit qui
permet de faire des tests en java. Il supporte

-  les tests automatiques,
-  les fonctions d'initialisation et de finalisation pour chaque test,
-  l'aggrégation des tests,
-  l'indépendance des tests dans le rapport final.

Pour écrire des tests, il faut respecter certaines règles.

-  Les tests doivent faire partie d'une classe héritée de la classe
   ``unittest.TestCase``.
-  Les noms des méthodes de cette classe doivent avoir le prefixe test
   pour être considérés comme tests.
-  Les tests sont exécutés par ordre alphabétique.
-  La fonction exécutée avant chaque test doit avoir le nom ``setUp``.
-  La fonction exécutée après chaque test doit avoir le nom
   ``tearDown``.

Voici un exemple de son utilisation avec notre module *fibonacci*.

.. literalinclude:: ../tests/test_fibonacci.py
    :linenos:


.. parsed-literal::

    python test_fibonacci.py


.. parsed-literal::

    test_coef_binomial (__main__.TestFibo) ... ok
    test_factorielle_0 (__main__.TestFibo) ... ok
    test_factorielle_5 (__main__.TestFibo) ... ok
    test_fibo (__main__.TestFibo) ... ok
    test_somme (__main__.TestFibo) ... ok

    ----------------------------------------------------------------------
    Ran 5 tests in 0.001s

    OK


Les assertions
--------------

Ils permettent de dire à **unittest** ce que l'on attend comme résultat
du test.

- ``assertEqual``: les 2 valeurs doivent être égales.
- ``assertAlmostEqual``: les 2 valeurs doivent être à peu près égales.
- ``assertTrue``: l'expression doit être vraie.
- ``assertFalse``: l'expression doit être fausse.
- ...

.. literalinclude:: ../tests/test_assert.py
    :linenos:


.. parsed-literal::

    python test_assert.py


.. parsed-literal::

    test_almostEqual (__main__.TestAssert) ... ok
    test_equal (__main__.TestAssert) ... ok
    test_false (__main__.TestAssert) ... ok

    ----------------------------------------------------------------------
    Ran 3 tests in 0.000s

    OK


Rassembler les tests
--------------------

.. literalinclude:: ../tests/all_tests.py
    :linenos:


.. parsed-literal::

    python all_tests.py


.. parsed-literal::

    test_almostEqual (test_assert.TestAssert) ... ok
    test_equal (test_assert.TestAssert) ... ok
    test_false (test_assert.TestAssert) ... ok
    test_coef_binomial (test_fibonacci.TestFibo) ... ok
    test_factorielle_0 (test_fibonacci.TestFibo) ... ok
    test_factorielle_5 (test_fibonacci.TestFibo) ... ok
    test_fibo (test_fibonacci.TestFibo) ... ok
    test_somme (test_fibonacci.TestFibo) ... ok

    ----------------------------------------------------------------------
    Ran 8 tests in 0.001s

    OK


Plus de documentation ici:
`unittest <https://docs.python.org/3.4/library/unittest.html>`_

nosetests
=========

Ce module reconnaît automatiquement les tests réalisés à partir de
**unittest** ou de **doctest**. Il a en plus d'autres fonctionalités
intéressantes

-  tests de couverture,
-  tests de profiling,
-  possibilité d'ajouter des plugins,
-  ...

Exemple d'utilisation avec doctest
----------------------------------

.. parsed-literal::

    nosetests -v --with-doctest fibonacci.py


.. parsed-literal::

    Doctest: fibonacci.coef_binomial ... ok
    Doctest: fibonacci.factorielle ... ok
    Doctest: fibonacci.fibonacci ... ok
    Doctest: fibonacci.somme ... ok

    ----------------------------------------------------------------------
    Ran 4 tests in 0.004s

    OK


Exemple d'utilisation avec unittest
-----------------------------------

.. parsed-literal::

    nosetests -v test_fibonacci.py


.. parsed-literal::

    test_coef_binomial (test_fibonacci.TestFibo) ... ok
    test_factorielle_0 (test_fibonacci.TestFibo) ... ok
    test_factorielle_5 (test_fibonacci.TestFibo) ... ok
    test_fibo (test_fibonacci.TestFibo) ... ok
    test_somme (test_fibonacci.TestFibo) ... ok

    ----------------------------------------------------------------------
    Ran 5 tests in 0.001s

    OK


Ecriture de tests avec nosetest
-------------------------------

**nosetests** considère qu'un fichier, un répertoire contient des tests
si celui-ci satisfait l'expression régulière que l'on appellera dans la
suite *matchTest*

::

    ((?:^|[\\b_\\.-])[Tt]est

En d'autres termes, il faut que les noms test ou Test soient au début du
nom ou qu'ils soient précédés de - ou \_.

La règle s'applique également aux fonctions, classes, ... qui se
trouvent dans le fichier à tester.

Exemples
--------

.. literalinclude:: ../tests/test_nose.py
    :linenos:


.. parsed-literal::

    nosetests -v test_nose.py


.. parsed-literal::

    test_nose.test_une_classe.encore_un_test_valide ... ok
    test_nose.test_une_classe.test_valide ... ok
    test_nose.ou_une_fonction_Test ... ok

    ----------------------------------------------------------------------
    Ran 3 tests in 0.001s

    OK


Les assertions
--------------

Tout comme **unittest**, **nosetests** comprend tout un tas d'outils
pour faire des assertions. Ils se trouvent dans ``nose.tools``.

Attention: contrairement à **unittest**, **nosetests** satisfait les
règles de la `PEP
8 <https://www.python.org/dev/peps/pep-0008#function-names>`__. Par
conséquent, ``assertEqual`` devient ``assert_equal``.

Initialisation et finalisation des tests
----------------------------------------

Il est possible d'exécuter du code en début et en fin de tests comme
avec les fonctions ``setUp`` et ``tearDown`` de **unittest**.

Pour les packages de tests
--------------------------

On peut ajouter les fonctions d'initialisation et de finalisation dans
le fichier ``__init__.py``.

Les fonctions d'initialisation doivent se nommer ``setup``,
``setup_package``, ``setUp`` ou ``setUpPackage``.

Les fonctions de finalisation doivent se nommer ``teardown``,
``teardown_package``, ``tearDown`` ou ``tearDownPackage``.

Pour les modules de tests
-------------------------

Un module de tests est un module dont le nom satisfait *matchTest*.

De la même manière que pour les packages, les fonctions d'initialisation
et de finalisation doivent avoir les noms ``setup``, ``setup_module``,
``setUp`` ou ``setUpModule`` et ``teardown``, ``teardown_module``,
``tearDown`` ou ``tearDownModule``

Pour les classes de tests
-------------------------

Une classe de tests est une classe dont le nom satisfait *matchTest*.
Elle doit se trouver dans un module de tests.

Soit elle dérive de la classe ``unittest.TestCase``, soit elle comporte
des méthodes qui satisfont le *matchTest*.

Les fonctions d'initialisation sont ``setup_class``, ``setupClass``,
``setUpClass``, ``setupAll`` ou ``setUpAll``.

Les fonctions de finalisation sont ``teardown_class``,
``teardownClass``, ``tearDownClass``, ``teardownAll`` ou
``tearDownAll``.

Pour les fonctions de tests
---------------------------

Une fonction de tests est une fonction dont le nom satisfait
*matchTest*. Elle doit se trouver dans un module de tests.

Contrairement aux précédents cas, il n'y a pas de format particulier
pour les phases d'initialisation et de finalisation.

Il faut utiliser le décorateur ``with_setup`` qui se trouve dans le
module ``nose``.

.. literalinclude:: ../tests/test_nose2.py
    :linenos:


.. parsed-literal::

    nosetests -s test_nose2.py


.. parsed-literal::

    j'initialise !!
    test 1
    je finalise !!
    .
    ----------------------------------------------------------------------
    Ran 1 test in 0.000s

    OK


Les attributs
-------------

Dans **nosetests**, il est possible de sélectionner une partie des tests
en utilisant des attributs.

On peut par exemple exécuter que les tests qui ne sont pas lents.

.. literalinclude:: ../tests/test_nose_attr.py
    :linenos:


.. parsed-literal::

    nosetests -s test_nose_attr.py


.. parsed-literal::

    test lent
    .test rapide
    .
    ----------------------------------------------------------------------
    Ran 2 tests in 0.000s

    OK


.. parsed-literal::

    nosetests -s -a '!lent' test_nose_attr.py


.. parsed-literal::

    test rapide
    .
    ----------------------------------------------------------------------
    Ran 1 test in 0.000s

    OK


.. literalinclude:: ../tests/test_nose_attr2.py
    :linenos:


.. parsed-literal::

    nosetests -s -a vitesse='lent' test_nose_attr.py


.. parsed-literal::

    test lent
    .
    ----------------------------------------------------------------------
    Ran 1 test in 0.000s

    OK


Les tests de couverture
-----------------------

Il est également possible avec **nosetests** de voir si nos tests
passent bien sur l'ensemble de notre code.

.. parsed-literal::

    nosetests --with-coverage --cover-package=fibonacci test_fibo.py


.. parsed-literal::

    .....
    Name        Stmts   Miss  Cover   Missing
    -----------------------------------------
    fibonacci.py   23      2    91%   66-67
    ----------------------------------------------------------------------
    Ran 5 tests in 0.002s

    OK


On voit qu'on a oublié de tester 2 lignes de notre module. Si on creuse
un peu ces 2 lignes correspondent aux lignes du main ce qui n'est donc
pas très important.

Exercices
=========

Nous allons utiliser les outils vus dans le cours
(**doctest**, **unittest** et **nosetests**) sur un package permettant
de simuler une calculatrice.

Il faut récupérer les sources qui se trouvent dans ``tests/tests_squelette``.
Pour travailler sur le projet sans l'installer, nous allons utiliser
la commande pip.

::

    cd tests/tests_squelette
    pip install -e .

Vous avez alors un exécutable qui s'appelle ``calc.py``

.. parsed-literal::

    calc.py


.. parsed-literal::


    usage
    =====

    ./calc.py s

    où s est une chaine de caractères représentant l'expression à
    calculer.



.. parsed-literal::

    calc.py '3+4-5'


.. parsed-literal::

    2.0


Ce package contient trois modules

-  *operateur*: ce module explique ce que fait une addition, une
   soustraction, ...
-  *parser*: ce module permet de parser la chaîne de caractères.
-  *calculatrice*: ce module permet de faire fonctionner la calculatrice
   en fonction de la chaîne de caractères entrée par l'utilisateur.

Lorsque l'on écrit un package en Python, on suit généralement
l'arborescence suivante pour les tests.

.. image:: _static/tests_arbo.png


1. Reprendre les modules *operateur* et *parser* et ajouter des tests en
   utilisant **doctest**.
2. Créer un répertoire de tests et écrire des tests utilisant
   **unittest**
3. Exécuter les tests créés précedemment avec **nosetests**
4. Ecrire des tests ayant le *matchTest* de **nosetests** dans le
   répertoire tests.
5. Ajouter des attributs et tester.
6. Faire des tests de couverture et rajouter des tests si nécessaire.
