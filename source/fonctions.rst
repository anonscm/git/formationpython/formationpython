=============
Les fonctions
=============

Définition
==========

Une fonction représente un traitement dédié à une tâche
particulière:

- quand on appelle une fonction, on quitte le programme principal pour
  aller dans la fonction
- on revient dans le programme principal à la fin de la fonction

Idéalement, une fonction ne doit pas dépasser 20 à 30 lignes.

.. code:: python

    # définition
    def ma_fonction():
        """Cette fonction ne fait rien et ne produit aucun résultat"""
        pass
    
    # Appel de la fonction
    ma_fonction()
    
    # Appel et affichage du résultat
    print(ma_fonction())

Résultat

.. parsed-literal::

    None


Avec paramètres:

.. code:: python

    def ma_fonction(a, b):
        """Cette fonction ne fait rien et ne produit aucun résultat"""
        pass

Une fonction contient: 

- le mot clef **def**
- un nom
- **(**
- des paramètres
- **):**
- le corps de la fonction
  
L'exécution d'une fonction se termine lorsque l'on termine le block indenté 
ou au premier **return**.

Nom des fonctions
=================

Des conventions de nommage et de codage sont définies dans la 
`PEP 8 <http://www.python.org/dev/peps/pep-0008/>`_ (site en anglais). C'est
d'ailleurs possible de mettre en forme automatiquement vos programmes python
en utilisant **autopep8**.

.. parsed-literal::

    autopep8 --in-place --aggressive --aggressive moche.py


En bref, il est recommandé d'utiliser que des minuscules avec les mots séparés
par un souligné (_).

Des bons exemples:

- calcul()
- mon_travail()

D'autres à éviter:

- Affiche()
- GetName()

Corps de la fonction
====================

Modification du corps de la fonction.

.. code:: python

    def ma_fonction():
        print("Dans ma fonction")

    # Appel à ma_fonction
    ma_fonction()

Resultat:

.. parsed-literal::

    Dans ma fonction


.. code:: python

    # Appel et affichage du résultat
    print(ma_fonction())


.. parsed-literal::

    Dans ma fonction
    None

Retour de données
=================

.. code:: python

    def ma_fonction():
        print("Dans ma fonction")
        return
        print("Toujours dans ma fonction")
    
    print(ma_fonction())

Résultat:

.. parsed-literal::

    Dans ma fonction
    None


.. code:: python

    def ma_fonction(a, b):
        resultat = int(a*b)
        print(a, "x", b, "=>", resultat)
        return resultat

    val = ma_fonction(10, 4.2)
    print("Le résultat de ma_fonction(10, 4.2) est:", val)


.. parsed-literal::

    10 x 4.2 => 42
    Le résultat de ma_fonction(10, 4.2) est: 42


Passage de paramètres
=====================

.. code:: python

    def ma_fonction(a, b):
        print("coucou", a, "et", b)

    ma_fonction("tata", "toto")

Résultat

.. parsed-literal::

    coucou tata et toto
    None


Python passe les variables par adresse.

.. code:: python

    def ma_fonction(a, b):
        print("{0} => 5, \"{1}\" => \"tata\"".format(a[0], "".join(b)))
        a[0] = 5
        b[1] = "a"
        b[3] = "a"

    a = [10]
    b = list("toto")
    ma_fonction(a, b)
    print("a=", a[0], ",", "b=", "".join(b))


.. parsed-literal::

    10 => 5, "toto" => "tata"
    a= 5 , b= tata


Cependant, les types scalaires de Python entier, flottant et chaine 
sont *immuables*.

.. code:: python

    def ma_fonction(a, b):
        print("{0} => 5, \"{1}\" => \"tata\"".format(a, b))
        a = 5
        b = "tata"

    a = 10
    b = "toto"
    ma_fonction(a, b)
    print("a=", a, ",", "b=", b)


.. parsed-literal::

    10 => 5, "toto" => "tata"
    a= 10 , b= toto


Pour communiquer les changements locaux à la fonction dans le programme
appelant, il faut les retourner à l'aide de **return**.

.. code:: python

    def ma_fonction(a, b):
        print("{0} => 5, \"{1}\" => \"tata\"".format(a, b))
        a = 5
        b = "tata"
        return(a, b)

    a = 10
    b = "toto"
    a, b = ma_fonction(a, b)
    print("a=", a, ",", "b=", b)

.. parsed-literal::

    10 => 5, "toto" => "tata"
    a= 5 , b= tata


Paramètres nommés
-----------------

Pour plus de lisibilité ou parce que ça vous dit, il est possible de
nommer les paramètres d'une fonction.

.. code:: python

    def ma_fonction(a, b, c):
        print(a, b, c)

    ma_fonction(c="a", a="b", b="c")

Résultat:

.. parsed-literal::

    b c a


Cela permet également de changer l'ordre des arguments de la fonction.

Valeurs par défaut
------------------

Il est possible de donner une valeur par défaut aux paramètres des
fonctions.

.. code:: python

    def ma_fonction(a, b=5, c=6):
        print(a, b, c)

    ma_fonction(1)
    ma_fonction(1, 2)
    ma_fonction(1, 2, 3)

Résultat:

.. parsed-literal::

    1 5 6
    1 2 6
    1 2 3


Récursivité
===========

Une fonction peut s'appeler elle-même, directement ou indirectement.

.. code:: python

    def fact(n):
        if n <= 1:
            return 1
        else:
            return n*fact(n-1)

    fact(5)

Résultat:

.. parsed-literal::

    120


Pensez à une condition d'arrêt. Attention aux boucles sans fin !

.. code:: python

    
    def inf(n):
        return inf(n + 1)

    try:
        inf(0)
    except RuntimeError as e:
        print("Erreur: \"{0}\"".format(e))


.. parsed-literal::

    Erreur: "maximum recursion depth exceeded"


Visibilité des variables
========================

- Chaque fonction crée sa propre pile
- Une fonction a accès à ses variables locales et aux variables globales
- Si une variable locale a le même nom qu'une variable globale, c'est
  la variable locale qui sera accessible
- Pour changer la valeur d'une variable globale dans une fonction, il
  faut utiliser le mot clef **global**

.. code:: python

    """Une fonction voit l'espace mémoire "global",
    mais ne le modifie pas.
    """
    a = 5
    b = 6
    c = 7
    def ma_fonction(c):
        b = 7
        print("a={0}, b={1}, c={2}".format(a, b, c))

    ma_fonction("$")
    print("a={0}, b={1}, c={2}".format(a, b, c))

Résultat:

.. parsed-literal::

    a=5, b=7, c=$
    a=5, b=6, c=7


.. code:: python

    """Partage de l'espace mémoire "global"
    """
    a = 5
    b = 6
    c = 7
    def ma_fonction(c):
        global b
        a = 8
        b = 7
        print("a={0}, b={1}, c={2}".format(a, b, c))

    ma_fonction("$")
    print("a={0}, b={1}, c={2}".format(a, b, c))

Résultat:

.. parsed-literal::

    a=8, b=7, c=$
    a=5, b=7, c=7


.. code:: python

    """Imbrication des espaces mémoires.
    """
    a = 5
    b = 6
    c = 7

    def ma_fonction(c):
        global b
        a = 8
        b = 7
        print("* a={0}, b={1}, c={2}".format(a, b, c))

        def ma_sous_fonction():
            global a
            b = 32
            a = 0
            print("** a={0}, b={1}, c={2}".format(a, b, c))
        ma_sous_fonction()

    ma_fonction("$")
    print("a={0}, b={1}, c={2}".format(a, b, c))

Résultat:

.. parsed-literal::

    * a=8, b=7, c=$
    ** a=0, b=32, c=$
    a=0, b=7, c=7


Paramètres définis à la volée
=============================

Il est possible pour une fonction d'accepter n'importe quel
argument (**déconseillé**).

Nombre variable d'arguments
---------------------------

.. code:: python

    def une_nouvelle_fonction(*arguments):
        for arg in arguments:
            print(arg, )
        print()

    une_nouvelle_fonction("abcd", 0, 2, [1, 2, 3, 4], {"a": 5, "b": 6})

Résultat

.. parsed-literal::

    abcd 0 2 [1, 2, 3, 4] {'a': 5, 'b': 6} 


Il est possible de passer une collection.

.. code:: python

    une_nouvelle_fonction(*"abcd")
    une_nouvelle_fonction(*[1, 2, 3, 4])
    une_nouvelle_fonction(*{"a": 5, "b": 6})

Résultat:

.. parsed-literal::

    a b c d 
    1 2 3 4 
    a b 


Avec des paramètres nommés:

.. code:: python

    def une_nouvelle_fonction(**arguments):
        for clef in arguments:
            print("{0}:{1}".format(clef, arguments[clef]), )
        print()

    une_nouvelle_fonction(a=5, b="6")

Résultat

.. parsed-literal::

    a:5 b:6

Il est également possible de passer une collection.

.. code:: python

    une_nouvelle_fonction(**{"a": 9, "b": 16})

Résultat:

.. parsed-literal::

    a:9 b:16


Pour aller plus loin
====================

Une fonction est une valeur comme une autre
-------------------------------------------

.. code:: python

    def fa(a):
        return a ** 2

    ma_var = fa
    print(ma_var(5))

Résultat:

.. parsed-literal::

    25


.. code:: python

    def accepte_une_fonction(f, v):
        return int(f(v))
    print(accepte_une_fonction(ma_var, 6.48074069841))
    print(accepte_une_fonction(fa, 6.48074069841))

Résultat:

.. parsed-literal::

    42
    42

Une fonction peut créer une autre fonction
------------------------------------------

Cette fonction "capture" les variables.

.. code:: python

    def f0(a):
        def f0_0():
            return a*2
        return f0_0

    ma_var2 = f0(21)
    ma_var3 = f0(42)
    print(ma_var2())
    print(ma_var3())

Résultat:

.. parsed-literal::

    42
    84


Syntaxe courte: les lambdas
---------------------------

.. code:: python

    ma_var4 = lambda a : a * 2
    print(ma_var4(21))

Résultat:

.. parsed-literal::

    42


Les first-class functions
-------------------------

    En informatique, un langage de programmation possède des
    *first-class functions* s'il traite une fonction comme tous ses
    autres types.

Les prédicats
~~~~~~~~~~~~~

Un prédicat est une fonction qui retourne "Vrai" ou "Faux" en fonction
de critères qui lui sont propres. Idéalement, la valeur "Vrai" ou "Faux"
du prédicat ne doit dépendre que des arguments de celui-ci.

.. code:: python

    """Prédicat qui nous dit si la valeur de l'entier a est plus grande que 10
    """
    def au_dessus_de_10(a):
        return a >= 10

    print(au_dessus_de_10(5), au_dessus_de_10(11))

Résultat:

.. parsed-literal::

    False True


Utilisation
~~~~~~~~~~~

.. code:: python

    mon_tableau = [1764, 49, 25, 9, 0, -16, 25, -9, 4, 64]
    print(list(filter(au_dessus_de_10, mon_tableau)))


.. parsed-literal::

    [1764, 49, 25, 25, 64]


Prédicats anonymes: les lambdas
===============================

.. code:: python

    mon_tableau = [1764, 49, 25, 9, 0, -16, 25, -9, 4, 64]
    resultat = filter(lambda a: a<0, mon_tableau)
    print(list(resultat))
    # exemple d'utilisation:
    for i in resultat:
        print(i)
    # ou encore:
    [i for i in resultat]

Résultat du **print**:

.. parsed-literal::

    [-16, -9]


Syntaxe: **lambda** arg0 [, arg1, [arg2, ...]] : **code**

La dernière *expression* de la section **code** de lambda correspond à
la valeur retournée.

.. code:: python

    lambda a: a<0
    print(list(filter(lambda a: a<0, mon_tableau)))

Résultat:

.. parsed-literal::

    [-16, -9]


.. code:: python

    # est équivalent à
    def inf_a_0(a):
        return a<0
    print(list(filter(inf_a_0, mon_tableau)))

Résultat:

.. parsed-literal::

    [-16, -9]


Traitement de données par fonctions
===================================

Il existe plusieurs traitements standards sur les données de type
**sequences** en python:

- filter(): qui, comme nous l'avons vu, permet de filtrer nos données selon un 
            critère exprimé par un prédicat
- map(): qui applique un traitement à chaque élément 

map
---

.. code:: python

    def un_exemple(a):
        if a > 0:
            return a+42
        else:
            return a-100 

    mon_tableau = [1764, 49, 25, 9, 0, -16, 25, -9, 4, 64]
    data = map(un_exemple, mon_tableau)
    print(list(data))

Résultat:

.. parsed-literal::

    [1806, 91, 67, 51, -100, -116, 67, -109, 46, 106]


Exercices
=========

Bonjour
-------

Écrire une fonction **affiche_bonjour** qui affiche "Bonjour".

Modifier cette fonction pour qu'elle prenne en paramètre **nom** et **prenom**
pour afficher::

    Bonjour UnNom UnPrénom


Opérations
----------

Écrire les 4 fonctions suivantes qui permettent respectivement de multiplier,
diviser, additionner et soustraire de nombre a et b:


- mul(a, b)
- div(a, b)
- add(a, b)
- sub(a, b)


Modifier la fonction dvi() afin de gérer l'erreur **ZeroDivisionError** et 
d'afficher dans ce cas soit **-inf** (-float("inf")), soit **+inf** 
(float("inf")).

Vous pourrez tester le bon fonctionnement de votre code avec les appels
suivants:

.. code:: python

    mul(21, 2)
    div(84, 2)
    div(4, 0)
    div(-3, 0)
    add(40, 2)
    sub(60, 18)


Correction
~~~~~~~~~~

.. code:: python

    def div(a, b):
        """Division de a par b.

        :param a: un entier
        :param b: un entier
        :return: un float, a / b
        """
        try:
            r = a / b
        except ZeroDivisionError:
            if a < 0:
                r = -float("inf")
            else:
                r = float("inf")
        return r

Listes
------

Les données suivantes seront utilisées pour l'exercice:

.. code:: python

    import random
    LIST_DATA = [4.1, 9, 0, "coucou", -1000, 1000, [ 0, 1], "42", 
        -float("inf"), 18, -49, -4999.0, -455, float("inf"), -541, 9999.9,
        float("nan")]
    LIST_DATA.extend(4.2 * random.randint(-1000, 1000) for r in range(20))
    CHAINE = "abcd0ef9ghij8klmA5BCD7EFG6HIJK4LMNO3PQR2STU1VWXYZnopqrstuvwxyz"


Écrire un filtre à l'aide d'une fonction lambda qui va sélectionner seulement
les données de type entières et flottantes dans LIST_DATA.

Écrire un filtre à l'aide d'une fonction lambda qui va sélectionner seulement
les données comprise entre [-1000 et 1000[ dans LIST_DATA.

Utiliser **map()** sur le résultat précédent afin d'obtenir une liste de
valeurs absolues.

Écrire un filtre à l'aide d'une fonction lambda qui va sélectionner seulement
les caractères entre 'e' et 'n' et entre 'E' et 'N' dans CHAINE.

