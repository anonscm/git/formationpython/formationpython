===================
Programmation objet
===================

La définition de classes
========================

Généralités
-----------

La création d'une nouvelle classe peut être considérée comme la création
d'un nouveau type de données, défini par le développeur. Par convention,
les noms de classe sont généralement typographié en "CamelCase". Dans
l'exemple ci-dessous, l'exécution de l'instruction ``class`` provoque la
création d'un objet de type ``class``, et lui assigne comme nom
``Vecteur``. Comme n'importe quelle entité de Python, la classe créée
est placée dans un module (ici ``__main__`` puisque l'instruction
``class`` est soumise directement à l'interpréteur).

.. code:: python

    class Vecteur(object):
        pass

    print Vecteur


.. parsed-literal::

    <class '__main__.Vecteur'>


De même qu'un type prédéfini de Python (par exemple ``float`` ou
``list``) possède de multiples valeurs (par exemple ``3.14`` ou ``[]``),
une classe peut donner naissance à de multiples objets. On dit de chacun
de ces objets qu'il est une "instance" de la classe. Pour créer une
telle instance, on utilise le nom de la classe comme si c'était un nom
de fonction (utilisation de parenthèses). Dans l'exemple ci-dessous,
l'instruction provoque la création d'une instance de type ``Vecteur``,
et lui assigne comme nom ``v``.

.. code:: python

    v = Vecteur()

    print v


.. parsed-literal::

    <__main__.Vecteur object at 0x00000000042B80F0>


Données membres
---------------

Tout comme un nombre flottant est composé de sous-parties (mantisse,
exposant, signe...), une instance est appelée à contenir des données
nommées, accessibles indivuellement par l'opérateur ``.``. Les données
membres peuvent être lues, créées et modifiées à tout moment et par
n'importe qui.

.. code:: python

    v.x = 1
    v.y = 2
    print v.x


.. parsed-literal::

    1


.. code:: python

    print v.y


.. parsed-literal::

    2


.. code:: python

    del v.y
    print v.y


::


    ---------------------------------------------------------------------------
    AttributeError                            Traceback (most recent call last)

    <ipython-input-5-3a2cca00cd27> in <module>()
          1 del v.y
    ----> 2 print v.y


    AttributeError: 'Vecteur' object has no attribute 'y'


Méthodes
--------

Imaginons ci-dessous que l'on souhaite doter nos nouveaux vecteurs d'un
calcul de norme et d'une transformation de type homothétie. Pour
uniformiser, on définit également une fonction pour initialiser les
attributs d'une instance, et une fonction d'affichage :

.. code:: python

    class Vecteur(object):
        pass

    def init(v,x=0,y=0):
        v.x = x
        v.y = y

    def norme(v):
        x2 = v.x**2
        y2 = v.y**2
        return (x2+y2)**(1./2.)

    def homothetie(v,n):
        v.x = v.x * n
        v.y = v.y * n

    def affiche(v):
        print v.x, v.y

    v = Vecteur()
    init(v,3,-4)
    print norme(v)


.. parsed-literal::

    5.0


.. code:: python

    homothetie(v,3)
    affiche(v)


.. parsed-literal::

    9 -12


On peut être rapidement confronté à des conflits de noms avec d'autres
fonctions similaires pour d'autres classes, et on aimerait que nos
nouvelles fonctions ne soient utilisées qu'avec notre nouvelle classe
Vector. Pour ce faire, nous allons en faire des *méthodes* en les
insérant dans la classe.

Une méthode est une fonction "attachée" à une classe particulière. La
définition des méthodes est faite à l'intérieur de la définition de la
classe.

Dans la définition d'une méthode, le premier paramètre désigne
l'instance courante sur laquelle on veut appliquer la méthode. Pour
exécuter une méthode sur une instance particuliere, on utilise
l'opérateur ".", comme pour les données membres, puis des parenthèses
pour signifier qu'il s'agit d'une fonction. L'instance à gauche du "."
est alors passée à la méthode en tant que premier paramètre.

.. code:: python

    class Vecteur(object):
        def init(self,x=0,y=0):
            self.x = x
            self.y = y
        def norme(self):
            x2 = self.x**2
            y2 = self.y**2
            return (x2+y2)**(1./2.)
        def homothetie(self,n):
            self.x = self.x * n
            self.y = self.y * n
        def affiche(self):
            print self.x, self.y


    v = Vecteur()
    v.init(3,-4)
    print v.norme()


.. parsed-literal::

    5.0


.. code:: python

    v.homothetie(3)
    v.affiche()


.. parsed-literal::

    9 -12


Encapsulation
-------------

Données membres et méthodes sont collectivement appelés "attributs". On
place généralement les données dans les instances (et elles varient
d'une instance à l'autre), par contreet les méthodes sont placées dans
la classe.

En programmation orientée objet orthodoxe, il est préconisé d'interdire
un accès direct aux données par les codes clients extérieurs, et de
forcer ces clients à passer par l'intermédiaire de méthodes de lecture
(qu'on appelle "getters") et de méthodes de modification (qu'on appelle
"setters").

Python ne permet pas vraiment d'interdire l'accès aux données, mais
seulement de les masquer. Pour ce faire, donnez leur un nom qui commence
par "\_\_". les données sont alors dites "pseudo-privées".

Dans l'exemple ci-dessous, les attributs ``x`` et ``y`` de ``Vector``
ont été rendus pseudo-privés, la méthode ``init()`` fait office de
"setter" global (méthode pour changer les valeurs de x et y), et les
méthodes ``getx()`` et ``gety()`` font office de "getters".

.. code:: python

    class Vecteur(object):

      def init(self,u=0,v=0):
        self.__x = u
        self.__y = v

      def getx(self):
        return self.__x

      def gety(self):
        return self.__y

    v = Vecteur()
    v.init(3, -4)
    print v.getx(), v.gety()


.. parsed-literal::

    3 -4


.. code:: python

    print v.x, v.y


::


    ---------------------------------------------------------------------------
    AttributeError                            Traceback (most recent call last)

    <ipython-input-17-9166e99d7851> in <module>()
    ----> 1 print v.x, v.y


    AttributeError: 'Vecteur' object has no attribute 'x'


.. code:: python

    print v.__x, v.__y


::


    ---------------------------------------------------------------------------
    AttributeError                            Traceback (most recent call last)

    <ipython-input-16-e70456a59924> in <module>()
    ----> 1 print v.__x, v.__y


    AttributeError: 'Vecteur' object has no attribute '__x'


Documentation
-------------

Les "docstrings" habituelles fonctionnent avec les classes.

.. code:: python

    class MaClasse(object):
        "Documentation de MaClasse"
        __data = []
        def ma_methode():
            "documentation de ma_methode()"
            pass

    print MaClasse.__doc__


.. parsed-literal::

    Documentation de MaClasse


.. code:: python

    print MaClasse.ma_methode.__doc__


.. parsed-literal::

    documentation de ma_methode()


.. code:: python

    help(MaClasse)


.. parsed-literal::

    Help on class MaClasse in module __main__:

    class MaClasse(__builtin__.object)
     |  Documentation de MaClasse
     |
     |  Methods defined here:
     |
     |  ma_methode()
     |      documentation de ma_methode()
     |
     |  ----------------------------------------------------------------------
     |  Data descriptors defined here:
     |
     |  __dict__
     |      dictionary for instance variables (if defined)
     |
     |  __weakref__
     |      list of weak references to the object (if defined)



Les méthodes spéciales
======================

Constructeur et destructeur
---------------------------

Le constructeur est une méthode spéciale, appelée ``__init__()``, qui
est appelée lors de la création d'une instance de classe.

Le destructeur est une méthode spéciale, appelée ``__del__()``, qui est
appelée lors de la destruction d'une instance. **ATTENTION** : on ne
contrôle pas vraiment le moment ou le destructeur est appelé. De ce
fait, il est rarement utilisé.

.. code:: python

    class Vector(object):
      def __init__(self,u=0,v=0):
        self.__x = u
        self.__y = v
      def getx(self):
        return self.__x
      def gety(self):
        return self.__y
      def norm(self):
        x2 = self.__x**2
        y2 = self.__y**2
        return (x2+ y2)**(1/2)

    v1 = Vector(3, 4)
    print v1.getx(), v1.gety()


.. parsed-literal::

    3 4


.. code:: python

    v2 = Vector(-2, 7)
    print v2.getx(), v2.gety()


.. parsed-literal::

    -2 7


Affichage
---------

Par défaut, l'affichage d'une instance renvoie son nom de classe et son
adresse en mémoire :

.. code:: python

    class Vector(object):
      def __init__(self,u=0,v=0):
        self.__x = u
        self.__y = v

    v = Vector(3,4)

    print v


.. parsed-literal::

    <__main__.Vector instance at 0x00000000054CC2C8>


.. code:: python

    v




.. parsed-literal::

    <__main__.Vector instance at 0x00000000054CC2C8>



On peut personnaliser cet affichage en s'appuyant sur les méthodes
spéciales ``__str()__`` et ``__repr()__``.

.. code:: python

    class Vector(object):
      def __init__(self,u=0,v=0):
        self.__x = u
        self.__y = v
      def __str__(self):
        return "(%.0f,%.0f)" % (self.__x,self.__y)
      def __repr__(self):
        return "Vector(%.0f,%.0f)" % (self.__x,self.__y)

    v = Vector(3,4)

    print v


.. parsed-literal::

    (3,4)


.. code:: python

    v




.. parsed-literal::

    Vector(3,4)



En cas d'affichage destiné à l'utilisateur, tel qu'un appel à ``print``,
c'est la méthode ``__str__`` qui est recherchée en priorité, et
seulement en dernier recours la méthode ``__repr__``. En principe, cette
dernière est supposé retourner un texte qui, si il était exécuté,
recréerait l'objet original.

Objets-fonctions
----------------

En dotant une classe de la méthode ``__call__``, les instances de cette
classe peuvent être utilisés comme des fonctions. On parle
d'objets-fonctions. Cela peut-être utile lorsqu'on veut utiliser un
programme générique qui attend une fonction en paramètre, et que l'on
veut lui donner à la place des objets qui savent se comporter en
fonction, tout en ayant des paramètres internes.

.. code:: python

    class Multiplier(object):
        def __init__(self,constant):
            self.constant = constant
        def __call__(self,other):
            return self.constant*other

    m2 = Multiplier(2)
    print m2(6)
    print map(m2,[1,2,3,4,5])


.. parsed-literal::

    12
    [2, 4, 6, 8, 10]


Ce mécanisme est souvent le plus sain pour associer des données
persistantes à une fonction, plutôt que l'utilisation de variables
globales, de fonctions imbriquées, ou de valeurs par défaut modifiables.

Opérateurs mathématiques
------------------------

Il est possible de permettre l'utilisation des opérateurs habituels avec
vos nouvelles classes, en redéfinissant des méthodes spéciales au nom
imposé :

-  ``+`` : ``__add__(self,other)``
-  ``-`` : ``__sub__(self,other)``
-  ``*`` : ``__mul__(self,other)``
-  ``/`` : ``__truediv__(self,other)``
-  ``//`` : ``__floordiv__(self,other)``
-  ``%`` : ``__mod__(self,other)``
-  ``**`` : ``__pow__(self,other)``

.. code:: python

    class Vector(object):
      def __init__(self,u=0,v=0):
        self.__x = u
        self.__y = v
      def __add__(self, other):
        if isinstance(other, Vector):
          newx = self.__x + other.__x
          newy = self.__y + other.__y
          return Vector(newx, newy)
      def __str__(self):
        return "(%.0f,%.0f)" % (self.__x,self.__y)

    v1 = Vector(2, 3)
    v2 = Vector(-4, 7)
    v3 = v1 + v2

    print v3


.. parsed-literal::

    (-2,10)


Index de séquence
-----------------

A l'aide de la méthode ``__getitem__()``, on peut implémenter
l'opérateur ``[]`` sous toutes ses formes :

.. code:: python

    class MonTexte(object):
        def __init__(self,value):
            self.data = value
        def __getitem__(self,index):
            return self.data[index]

    txt = MonTexte("Bonjour")
    print txt[2]


.. parsed-literal::

    n


.. code:: python

    print txt[-1::-1]


.. parsed-literal::

    ruojnoB


En bonus, la présence de ``__getitem__`` permet d'utiliser un objet au
sein d'une boucle ``for``. L'interpréteur va invoquer cette méthode avec
des valeurs entières croissantes, en commencant par ``0``, et jsuqu'à
l'émission d'une exception ``IndexError``.

for c in txt:
    print c

En réalité, la présence de ``__getitem__`` permet d'utiliser un objet
dans de multiples autres "contextes d'itération", tels que qu'un test
d'appartenance ``in``, une "compréhension de liste", la fonction
prédéfinie ``map``, l'assignation de séquences, ...

.. code:: python

    'i' in txt




.. parsed-literal::

    False



.. code:: python

    [c for c in txt]




.. parsed-literal::

    ['B', 'o', 'n', 'j', 'o', 'u', 'r']



.. code:: python

    map(ord,txt)




.. parsed-literal::

    [66, 111, 110, 106, 111, 117, 114]



.. code:: python

    (c1, c2, c3, c4, c5, c6, c7) = txt
    c3, c2, c1




.. parsed-literal::

    ('n', 'o', 'B')



.. code:: python

    list(txt), tuple(txt), '-'.join(txt)




.. parsed-literal::

    (['B', 'o', 'n', 'j', 'o', 'u', 'r'],
     ('B', 'o', 'n', 'j', 'o', 'u', 'r'),
     'B-o-n-j-o-u-r')



Les objets composés
===================

Généralités
-----------

Un objet peut contenir des objets, qui peuvent contenir des objets, qui…
On peut descendre dans les niveaux successifs en enchainant les "." .

.. code:: python

    class Vecteur(object): pass
    class Impulsion(object) : pass

    imp = Impulsion()
    imp.vitesse = Vecteur()
    imp.vitesse.x = 1.
    imp.vitesse.y = 0.
    imp.vitesse.z = 0.
    imp.masse = 10.

    print ( imp.vitesse.x, imp.vitesse.y, imp.vitesse.z, imp.masse )


.. parsed-literal::

    (1.0, 0.0, 0.0, 10.0)


(Non)Chainage des constructeurs
-------------------------------

Dans un constructeur, il n'y a pas d'appel automatique aux constructeurs
des sous-parties de l'objet courant, comme cela existe dans d'autres
langages. Un objet n'a pas d'obligation de contenir des données, et donc
tout ce qui est ajouté doit l'être explicitement, y compris dans les
constructeurs.

.. code:: python

    class Vecteur(object):
        def __init__(self):
            self.x = 0
            self.y = 0
            self.z = 0
        def __str__(self):
            return "%d|%d|%d" % ( self.x, self.y, self.z )

    class Impulsion(object):
        def __init__(self):
            self.vitesse = Vecteur()
            self.masse = 0
        def __str__(self):
            return "%s|%d" % ( self.vitesse.__str__(), self.masse )

    i1 = Impulsion()
    print "(%s)" % i1


.. parsed-literal::

    (0|0|0|0)


Du point de vue de l'affectation, de la copie, de la comparaison, et de
beaucoup d'autres opérations, un objet se comporte essentiellement comme
un dictionnaire contenant des attributs.

Affectation et copie
--------------------

Lorsqu'on affecte un objet à une variable, il n'y a pas de copie. Comme
d'habitude, cela génère une nouvelle variable qui référence le même
objet original. Dans l'exemple ci-dessous, on voit que si je modifie un
attribut de ``i2``, ``i1`` est également modifié.

.. code:: python

    i2 = i1
    i2.masse = 1
    print "(%s), (%s)" % ( i2, i1 )


.. parsed-literal::

    (0|0|0|1), (0|0|0|1)


Pour dupliquer un objet, on peut avoir recours au module ``copy``, et à
sa fonction ``copy``. Ainsi, si je modifie maintenant un attribut de
``i2``, l'attribut correspondant de ``i1`` est inchangé.

.. code:: python

    import copy
    i2 = copy.copy(i1)
    i2.masse = 2
    print "(%s), (%s)" % ( i2, i1 )


.. parsed-literal::

    (0|0|2|2), (0|0|2|1)


Mais il s'agit d'une copie de surface. L'équivalent d'une affectation
des attributs un à un :

.. code:: python

    i2 = Impulsion()
    i2.vitesse = i1.vitesse # i2 = copy.copy(i1)
    i2.masse = i1.masse     # i2 = copy.copy(i1)
    i2.masse = 2
    print "(%s), (%s)" % ( i2, i1 )


.. parsed-literal::

    (0|0|0|2), (0|0|0|1)


Comme au début, pour ``i2 = i1``, l'instruction
``i2.vitesse = i1.vitesse`` ne duplique pas l'instance de ``Vecteur``,
mais fait en sorte que ``i2.vitesse`` et ``i1.vitesse`` désigne la même
instance. Ainsi, si on change un attribut de ``i2.vitesse``, on agit
également sur ``i1.vitesse`` :

.. code:: python

    i2.vitesse.z = 2
    print "(%s), (%s)" % ( i2, i1 )


.. parsed-literal::

    (0|0|2|2), (0|0|2|1)


Si on souhaite dupliquer intégralement un objet et tous ses attributs à
tous les niveaux, on préfèrera la fonction ``deepcopy``. Dans l'exemple
suivant, on modifie un sous-sous-attribut de ``i2``, sans affecter
``i1``.

.. code:: python

    i2 = copy.deepcopy(i1)
    i2.vitesse.y = 3
    print "(%s), (%s)" % ( i2, i1 )


.. parsed-literal::

    (0|3|2|1), (0|0|2|1)


Egalité et identité
-------------------

Malheureusement, à la différence d'un dictionnaire, l'opérateur de
comparaison ``==`` n'est pas capable de vérifier récursivement l'égalité
de tous les données membres de l'objet. A la place, il se contente de
vérifier que les deux variables désignent le même objet, à la façon d'un
``is``. Pour obtenir le comportement "logique", il faut par exemple
redéfinir les opérateurs ``__eq__``\ et ``__ne__`` à tous les niveaux :

.. code:: python

    class Vecteur(object):
        def __init__(self,x=0,y=0,z=0):
            self.__x = x
            self.__y = y
            self.__z = z
        def __str__(self):
            return "%d|%d|%d" % ( self.__x, self.__y, self.__z )

    class Impulsion(object):
        def __init__(self,x=0,y=0,z=0,m=0):
            self.__vitesse = Vecteur(x,y,z)
            self.__masse = m
        def __str__(self):
            return "%s|%d" % ( self.__vitesse.__str__(), self.__masse )

    i1 = Impulsion(1,2,3,4)

    import copy
    i2 = copy.deepcopy(i1)

    (i1 is i2, i1==i2, i1!=i2)




.. parsed-literal::

    (False, False, True)



.. code:: python

    class Vecteur(object):
        def __init__(self,x=0,y=0,z=0):
            self.__x = x
            self.__y = y
            self.__z = z
        def __str__(self):
            return "%d|%d|%d" % ( self.__x, self.__y, self.__z )
        def __eq__(self,other):
            return ( self.__x==other.__x ) and ( self.__y==other.__y ) and ( self.__z==other.__z )

    class Impulsion(object):
        def __init__(self,x=0,y=0,z=0,m=0):
            self.__vitesse = Vecteur(x,y,z)
            self.__masse = m
        def __str__(self):
            return "%s|%d" % ( self.__vitesse.__str__(), self.__masse )
        def __eq__(self,other):
            return ( self.__vitesse==other.__vitesse ) and ( self.__masse==other.__masse )

    i1 = Impulsion(1,2,3,4)

    import copy
    i2 = copy.deepcopy(i1)

    (i1 is i2, i1==i2, i1!=i2)




.. parsed-literal::

    (False, True, True)



.. code:: python

    class Vecteur:
        def __init__(self,x=0,y=0,z=0):
            self.__x = x
            self.__y = y
            self.__z = z
        def __str__(self):
            return "%d|%d|%d" % ( self.__x, self.__y, self.__z )
        def __eq__(self,other):
            return ( self.__x==other.__x ) and ( self.__y==other.__y ) and ( self.__z==other.__z )
        def __ne__(self,other): return not(self==other)

    class Impulsion:
        def __init__(self,x=0,y=0,z=0,m=0):
            self.__vitesse = Vecteur(x,y,z)
            self.__masse = m
        def __str__(self):
            return "%s|%d" % ( self.__vitesse.__str__(), self.__masse )
        def __eq__(self,other):
            return ( self.__vitesse==other.__vitesse ) and ( self.__masse==other.__masse )
        def __ne__(self,other): return not(self==other)

    i1 = Impulsion(1,2,3,4)

    import copy
    i2 = copy.deepcopy(i1)

    (i1 is i2, i1==i2, i1!=i2)




.. parsed-literal::

    (False, True, False)



L'héritage entre classes
========================

Généralités
-----------

Python permet d'exprimer une relation "est une sorte de" entre deux
classes. Si ``B`` est une sorte de ``A``, on s'attend à ce que les
instances de ``B`` possède les attributs (données et méthodes) de cette
classe, mais aussi tous ceux de ``A``, puisque ces objets sont aussi une
sorte de ``A``. On dit que ``B`` hérite de ``A``, ou dérive de ``A``. On
peut aussi qualifier ``A`` de "classe mère", de "classe de base", de
"super-classe", et ``B`` de "classe dérivée", de "classe fille", de
"sous-classe" de ``A``.

Pour hériter d'une classe mère, il suffit d’indiquer son nom entre
parenthèses après le nom de la classe fille. La classe fille a accès à
tous les attributs de la classe mère. En d'autres termes, quand on écrit
``obj.x``, l'interpréteur cherche ``x`` dans l'instance, puis dans sa
classe, puis dans la classe mère de cette classe, etc.

La classe prédéfinie ``object`` sert de classe de base commune à toutes
les classes "nouveau style" de Python. Assurez-vous toujours que vos
classes héritent d'\ ``object``, directement ou indirectement, sans quoi
vous perdrez un grand nombre de fonctionnalités du Python moderne.

.. code:: python

    class A(object):
        def x(self): return "A"
        def y(self): return "A"

    class B(A):
        def z(self): return "B"

    b = B()

    print b.x(), b.y(), b.z()


.. parsed-literal::

    A A B


Surcharge
---------

Une classe dérivée peut ajouter de nouveaux attributs, qu'il s'agisse de
données ou de méthodes. Elle peut aussi redéfinir un des attributs de la
classe de base, auquel cas, pour les instances de la classe dérivée,
c'est la nouvelle définition qui sera trouvée la première, et utilisée.
On parle de "surcharge".

.. code:: python

    class A(object):
        def x(self): return "A"
        def y(self): return "A"
        def z(self): return "A"

    class B(A):
        def x(self): return "B"
        def t(self): return "B"

    b = B()

    print b.x(), b.y(), b.z(), b.t()


.. parsed-literal::

    B A A B


Héritage multiple
-----------------

Une classe peut hériter de plusieurs autres en même temps (on donne
alors une liste de noms séparés par des virgules). L'ordre de gauche à
droite des classes parentes est respecté lorsque l'interpréteur Python
recherche un nom (donnée ou méthode).

.. code:: python

    class A1(object):
        def x(self): print "A1"

    class A2(object):
        def x(self): print "A2"

    class B(A1,A2):
        pass

    b = B()
    b.x()


.. parsed-literal::

    A1


Arborescence d'héritage
-----------------------

En plus de l'héritage multiple, chaque classe de base peut à son tour
hériter d'une autre classe, etc. L'ensemble des classes dont hérite une
instance forme ainsi une arborescence parfois complexe. Dans cette
arborescence, la recherche d'un attribut se fait de bas en haut et de
gauche à droite, en profondeur d'abord, mais en faisant en sorte qu'une
classe de base ne soit jamais explorée avant que toutes ses dérivées le
soient. Ainsi, dans l'exemple ci-dessous, ``A`` est prioritaire sur
``C``, mais pas sur ``B2`` :

.. code:: python

    class A(object):
        def x(self): return "A"
        def y(self): return "A"
        def z(self): return "A"

    class B1(A):
        pass

    class B2(A):
        def x(self): return "B2"

    class C(object):
        def y(self): return "C"
        def z(self): return "C"

    class D(B1,B2,C):
        z = C.z

    d = D()
    print d.x(), d.y(), d.z()


.. parsed-literal::

    B2 A C


L'ordre de parcours des classes est appelé le **MRO** (Method Resolution
Order).

On voit aussi, dans l'exemple ci-dessus, que l'on peut explicitement
copier la méthode ``z`` de ``C`` dans ``D``. Ainsi, tout appel à la
méthode ``z`` pour une instance de ``D`` se trouve redirigé vers la
méthode de ``C`` (au lieu de ``A``).

Chaque recherche d'attribut est indépendante et repart de ``self``
------------------------------------------------------------------

Lorsque j'appelle une méthode m1 de l'objet obj, cette méthode est
recherchée dans l'arborescence de classes de obj. Si, à son tour, m1
appelle une autre méthode m2, la recherche de m2 repart de obj (et pas
de la classe de m1). En C++, on dirait que toutes les méthodes sont
virtuelles et toutes les variables polymorphiques.

.. code:: python

    class A(object):
        def x(self): return "A"
        def y(self): return "A"
        def affichex(self): print self.x()

    class B1(A):
        pass

    class B2(A):
        def x(self): return "B2"

    class C(object):
        def x(self): return "C"
        def affichey(self): print self.y()

    class D(B1,B2,C):
        pass

    d = D()
    d.affichex()
    d.affichey()


.. parsed-literal::

    B2
    A


Reutiliser explicitement une méthode de base
--------------------------------------------

Lorsqu'une méthode est redéfinie dans une classe dérivée, on dit qu'elle
est surchargée. Plutôt que de réécrire toutes les instructions, il peut
être utile de commencer par éxécuter le code de la méthode de la classe
de base. C'est particulièrement vrai pour les méthodes spéciales, telles
que les constructeurs.

.. code:: python

    class Fruit(object):
        def __init__(self, couleur, variete=''):
            self.__couleur= couleur
            self.__variete= variete
        def __str__(self):
            if (self.__variete!=''):
                return "%s %s" % (self.__variete,self.__couleur)
            else:
                return "fruit %s" % self.__couleur

    class Pomme(Fruit):
        def __init__(self, couleur, variete='golden'):
            Fruit.__init__(self,couleur,variete)
        def __str__(self):
            return ("pomme " + Fruit.__str__(self))

    p = Pomme('rouge')

    print p


.. parsed-literal::

    pomme golden rouge


On peut remplacer l'appel explicite à la classe de base par un appel à
la fonction prédéfinie ``super()``, en lui passant le nom de la classe
courante et l'instance courante. Dans le cas d'un héritage simple, cela
permet ensuite de revoir les classe de base sans avoir à corriger tous
les appels directs aux méthodes de la classe de base.

.. code:: python

    class Pomme(Fruit):
        def __init__(self, couleur, variete='golden'):
            super(Pomme,self).__init__(couleur,variete)
        def __str__(self):
            return ("pomme " + Fruit.__str__(self))

    p = Pomme('rouge')

    print p


.. parsed-literal::

    pomme golden rouge


Cependant, le nom ``super()`` est un faux ami, car en cas d'héritage
multiple, il y a plusieurs super-classes. En réalité ce que renvoie la
fonction ``super()``, c'est un objet intermédiaire, qui va rechercher
l'attribut demandé en suivant le chemin habituellement parcouru en cas
d'héritage (MRO).

.. code:: python

    class A(object):
        def x(self): return "A"

    class B1(A):
        pass

    class B2(A):
        def x(self): return "B2"

    class C(B1,B2):
        def x(self):
            return super(C,self).x()

    c = C()
    print c.x()


.. parsed-literal::

    B2


*PYTHON 3 SEULEMENT : on peut appeler super() sans arguments, ce qui
permet notamment de renommer une classe sans avoir à corriger tous les
appels à super().*

Sauvegarde et reconstruction d'objets
=====================================

Généralités
-----------

-  Les objets sont dit "persistants" lorsqu'ils ne disparaissent pas à
   la fin du programme Python en cours. Cela consiste en général à les
   "sérialiser", c'est à dire en produire une représentation binaire
   compacte, et à stocker cette représentation dans un fichier.
-  En Python, la persistance repose sur des modules de la libraire
   standard, tels que ``pickle`` et ``shelve``.

Pickle / cPickle
----------------

-  Ce module est capable de transformer en flot d'octets n'importe quel
   objet en mémoire. Ces octets peuvent être écrits dans un fichier,
   envoyés par réseau, etc.
-  A l'inverse, le module peut lire et recrée en mémoire les objets d'un
   fichier, l'un après l'autre.
-  Quand c'est disponible, préférez cPickle, écrit en C et plus rapide.
-  **ATTENTION** : pas de garantie de portabilité de vos données entre
   des plateformes différentes.

.. code:: python

    class Vecteur:
        def __init__(self,u=0,v=0):
            self.x = u
            self.y = v
        def __str__(self):
            return "(%.0f,%.0f)" % (self.x,self.y)

    v1 = Vecteur(1,2)
    v2 = Vecteur(3,4)

    import pickle

    fw = open('vecteurs.pkl','w')
    pickle.dump(v1,fw)
    pickle.dump(v2,fw)
    fw.close()

    fr = open('vecteurs.pkl','r')
    print pickle.load(fr)
    print pickle.load(fr)
    fr.close()


.. parsed-literal::

    (1,2)
    (3,4)


-  `documentation python
   2 <http://docs.python.org/2.7/library/pickle.html>`__
-  `documentation python
   3 <http://docs.python.org/3.3/library/pickle.html>`__

Shelve
------

-  Ce module permet de ranger des objets dans un fichier, non pas
   séquentiellement, mais en les associant à des noms.
-  Le fichier est manipulé comme si il s'agissait d'un dictionnaire.

.. code:: python

    class Vecteur:
        def __init__(self,u=0,v=0):
            self.x = u
            self.y = v
        def __str__(self):
            return "(%.0f,%.0f)" % (self.x,self.y)

    import shelve

    s = shelve.open('vecteurs.db')
    s['v1'] = Vecteur(1,2)
    s['v2'] = Vecteur(3,4)
    s.close()

    #...

    s = shelve.open('vecteurs.db')
    print s.keys()
    print s['v1']
    print s['v2']
    s.close()

-  `documentation python
   2 <http://docs.python.org/2.7/library/shelve.html>`__
-  `documentation python
   3 <http://docs.python.org/3.3/library/shelve.html>`__

Compléments
===========

Attributs cachés
----------------

Dans un objet, les attributs ordinaires sont stockés dans un attribut
spécial nommé ``__dict__``. Chaque instance possède également un
attribut nommé ``__class__`` qui pointe vers sa classe.

Dans une classe, on trouve également des attributs spéciaux : \*
``__doc__``: documentation de la classe, \* ``__dict__`` : attributs de
la classe, \* ``__name__`` : nom de la classe, \* ``__bases__`` : tuple
contenant les classes de bases de la classe courante. \* ``__mro__`` :
ordre de parcours des ancêtres de la classe. \* ``__module__`` : nom du
module où la classe est définie (``__main__`` en mode interactif).

.. code:: python

    class A1 : pass
    class A2 : pass

    class B(A1,A2):
        def __init__(self,value): self.data = value
        def display(self) : print "data:", self.data

    b = B("abc")

    print b.__dict__

.. code:: python

    print b.__dict__['data']


.. parsed-literal::

    abc


.. code:: python

    print ( b.__class__, )


.. parsed-literal::

    (<class __main__.B at 0x0000000004206588>,)


.. code:: python

    print b.__class__.__name__


.. parsed-literal::

    B


.. code:: python

    print b.__class__.__bases__


.. parsed-literal::

    (<class __main__.A1 at 0x0000000004180EE8>, <class __main__.A2 at 0x00000000042065E8>)


.. code:: python

    print b.__class__.__dict__


.. parsed-literal::

    {'__module__': '__main__', 'display': <function display at 0x000000000422A588>, '__init__': <function __init__ at 0x000000000422A5F8>, '__doc__': None}


Attributs pseudo-privés
-----------------------

Au sein d'une instruction composée ``class``, tous les noms qui sont
préfixés par un double souligné ``__`` (et non à la fin), sont
"magiquement" manipulés par l'interpréteur Python qui insère devant le
nom un simple souligné ``_`` et le nom de la classe courante. Ainsi, les
attributs recoivent un nom qui devient spécifique à la classe, et qui ne
risque plus d'être redéfini par erreur dans les classes dérivées. On
parle d'attributs "pseudo-privés", non pas parce que l'accès à ces
attributs est véritablement interdit, mais parce que l'altération
automatique de leurs noms complique leur accès de l'extérieur.

.. code:: python

    class Demo:
        public_data = "public data"
        __private_data = "private data"
        def public_method(self): print "public method()"
        def __private_method(self): print "private method()"

    dir(Demo)




.. parsed-literal::

    ['_Demo__private_data',
     '_Demo__private_method',
     '__doc__',
     '__module__',
     'public_data',
     'public_method']



.. code:: python

    print Demo.public_data


.. parsed-literal::

    public data


.. code:: python

    print Demo.__private_data


::


    ---------------------------------------------------------------------------
    AttributeError                            Traceback (most recent call last)

    <ipython-input-9-5a651a2f66b8> in <module>()
    ----> 1 print Demo.__private_data


    AttributeError: class Demo has no attribute '__private_data'


.. code:: python

    print Demo._Demo__private_data


.. parsed-literal::

    private data


.. code:: python

    d = Demo()
    d.public_method()


.. parsed-literal::

    public method()


.. code:: python

    d.__private_method()


::


    ---------------------------------------------------------------------------
    AttributeError                            Traceback (most recent call last)

    <ipython-input-24-f98a2e3c7104> in <module>()
    ----> 1 d.__private_method()


    AttributeError: Demo instance has no attribute '__private_method'


.. code:: python

    d._Demo__private_method()


.. parsed-literal::

    private method()


A la différence d'autres langages, toutes les données d'un objet réside
dans l'objet lui-même, quelle que soit la classe et la méthode à
l'origine de ces données. Cela peut générer des interactions
involontaires, en particulier dans le cas d'un héritage multiple venant
de classes issues de développeurs indépendants. Les attributs
pseudo-privés évitent ces interactions non voulues.

.. code:: python

    class C1(object):
        def set1(self): self.__X = 1
        def get1(self): return self.__X

    class C2(object):
        def set2(self): self.__X = 2
        def get2(self): return self.__X

    class C3(C1,C2): pass

    obj = C3()
    obj.set1()
    obj.set2()
    print obj.get1(), obj.get2()


.. parsed-literal::

    1 2


Propriétés
----------

On peut définir dans une classe des attributs particuliers de type
"propriété" (property). Ces propriétés ressemblent à des variables
membres, et s'utilisent comme des variables membres, mais en réalité le
fait de lire une propriété déclenche l'utilisation du "getter" qui lui
est attaché, et le fait d'affecter une nouvelle valeur à cette propriété
déclenche l'utilisation du "setter" attaché (si et seulement si il est
défini). Typiquement, "getter" et "setter" manipulent une variable
interne pseudo-privée.

Ci-dessous, nous définissons pour la classe ``Vector`` des propriétés
``x`` et ``y``, dont la lecture déclenche un appel à ``getx()`` ou
``gety()``, qui renvoient les valeurs des variables pseudo-privées
``__x`` ou ``__y`` des instances de ``Vector``. Seule la propriété ``x``
a été associée à un "setter". ``y``, en l'absence de "setter", est en
lecture seule : on ne peut pas lui affecter de valeur (par contre on
peut le faire sur ``__y`` via ``init()``).

.. code:: python

    class Vector(object):
      def init(self,u=0,v=0):
        self.__x = u
        self.__y = v
      def getx(self):
        return self.__x
      def setx(self,u):
        self.__x = u
      x = property(getx,setx)
      def gety(self):
        return self.__y
      y = property(gety)

    v = Vector()
    v.init(3, -4)
    print v.x, v.y
    v.x = 2
    print v.x


.. parsed-literal::

    3 -4
    2


.. code:: python

    v.y = 5


::


    ---------------------------------------------------------------------------
    AttributeError                            Traceback (most recent call last)

    <ipython-input-29-916c9ff1d97a> in <module>()
    ----> 1 v.y = 5


    AttributeError: can't set attribute


Ce qui est remarquable ici, c'est que Python ne force pas une
encapsulation précoce et inutile. On peut très bien développer une
première version de classe avec des attributs publics, avoir des
clients, puis décider ultérieurement d'en faire des propriétés, si le
besoin s'en fait sentir.

On a vu aussi, dans notre exemple, qu'il est facile de faire des
propriétés "read-only" en ne déclarant que les "getters". Il est
cependant plus classique de fournir à la fois la méthode de lecture et
d'écriture, notamment pour controler qu'on n'enregistre que des valeurs
valides dans une propriété. Par exemple, si nos vecteurs ne peuvent
contenir que des valeurs entre -1 et 1 :

.. code:: python

    class Vector(object):
      def init(self,u=0,v=0):
        self.x = u
        self.y = v
      def getx(self):
        return self.__x
      def setx(self,x):
        if (x<-1): self.__x = -1
        elif (x>1): self.__x = 1
        else: self.__x = x
      x = property(getx,setx)
      def gety(self):
        return self.__y
      def sety(self,y):
        if (y<-1): self.__y = -1
        elif (y>1): self.__y = 1
        else: self.__y = y
      y = property(gety,sety)

    v = Vector()
    v.init(3, -4)
    print v.x, v.y


.. parsed-literal::

    1 -1


Variables de classe
-------------------

On peut ajouter des données à une classe (et pas seulement des
méthodes), auquel cas elles sont en quelque sorte partagées et visibles
pour toutes les instances de la classe.

En effet, quand vous demandez à lire la donnée nommée ``x`` de
l'instance ``obj``, ce qui se note ``obj.x``, l'interpréteur Python
cherche d'abord le nom dans l'instance elle-même, puis à défaut dans la
classe, puis dans ses ancêtres, comme pour n'importe quel attribut.

Dans l'exemple ci-dessous, on dote la classe ``Vecteur`` de variable
``x`` et ``y`` qui contiennent des valeirs par défaut pour les
instances. On peut créer ces variables directement lors de la création
de classe, comme nous le faisons pour ``x``, ou l'ajouter
ultérieurement, comme nous le faisons pour ``y``.

.. code:: python

    class Vecteur(object) :
        x = 0

    Vecteur.y = 0

    v = Vecteur()

    print Vecteur.x, Vecteur.y
    print v.x, v.y


.. parsed-literal::

    0 0
    0 0


Par contre, si vous affectez une nouvelle valeur à ``obj.x``, et que ce
nom n'existe pas encore dans l'instance, un nouvel attribut est créé à
cette occasion, dans l'instance. Formulons le à nouveau : en cas de
lecture, l'interpréteur cherche l'attribut dans l'instance concernée,
puis dans sa classe et ses ancêtres, mais en cas d'affectation d'une
nouvelle valeur, l'attribut est ajouté à l'instance si il n'existe pas
déjà.

.. code:: python

    v.x, v.y = 10, 20

    print Vecteur.x, Vecteur.y
    print v.x, v.y


.. parsed-literal::

    0 0
    10 20


Un langage très dynamique
-------------------------

Les instructions ``class`` et les instructions ``def``, sont traitées à
l'exécution comme n'importe quelles instructions. Elles retournent des
objets, certes un peu spéciaux, et leur assigne un nom dans l'espace de
nom courant. Avec ces objets, on peut réaliser des manipulations
impossibles dans d'autres langages, comme par exemple créer une méthode
d'abord en tant que fonction, à l'extérieur de la classe, puis la
rattacher à posteriori.

.. code:: python

    class A(object):
        def __init__(self,value): self.data = value
        def display(self) : print "data:", self.data

    a = A('bonjour')

    def my_display_upper(self):
        print "data:", self.data.upper()

    A.display_upper = my_display_upper

    a.display_upper()


.. parsed-literal::

    data: BONJOUR


Une méthode peut s'invoquer à travers le nom de classe, mais dans ce cas
il ne faut pas oublier de redonner le nom de l'objet à traiter comme
premier argument (self) :

.. code:: python

    A.display_upper(a)


.. parsed-literal::

    data: BONJOUR


Je peux toujours aussi appeler directement la fonction à travers son nom
original (``my_display_upper``) :

.. code:: python

    my_display_upper(a)


.. parsed-literal::

    data: BONJOUR


On peut même s'amuser, comme ci-dessous, à décrocher la fonction de la
classe (en effacant le nom), et à l'accrocher directement à l'instance.
Mais dans ce cas, la définition automatique de ``self`` n'est pas
réalisée (ce mécanisme n'est actif que pour les fonctions attachées dans
des classes). On doit alors redonner ``a`` comme argument à l'appel de
fonction.

.. code:: python

    del A.display_upper

    a.display_upper = my_display_upper

    a.display_upper(a)


.. parsed-literal::

    data: BONJOUR


Les encoches
------------

La définition dans une classe d'un attribut spécial nommé ``__slots__``
permet de limiter la liste des attributs autorisés (sans pour autant les
créer). Cela permet d'éviter des erreurs de frappe involontaire, de
faire certaines optimisations, mais en contre-partie cela peut affecter
l'existence de l'attribut spécial ``__dict__``, et perturber le
fonctionnement d'outils génériques qui s'appuient sur ce dictionnaire
interne.

.. code:: python

    class DemoSlots(object):
        __slots__ = ['att1','att2']

    ds = DemoSlots()
    print ds.att1


::


    ---------------------------------------------------------------------------
    AttributeError                            Traceback (most recent call last)

    <ipython-input-26-c8617deaf333> in <module>()
          3
          4 ds = DemoSlots()
    ----> 5 print ds.att1


    AttributeError: att1


.. code:: python

    ds.att1 = "this is att1"
    print ds.att1


.. parsed-literal::

    this is att1


.. code:: python

    ds.att3 = "this is att3"


::


    ---------------------------------------------------------------------------
    AttributeError                            Traceback (most recent call last)

    <ipython-input-40-3f4ad9ce7b2c> in <module>()
    ----> 1 ds.att3 = "this is att3"


    AttributeError: 'DemoSlots' object has no attribute 'att3'


.. code:: python

    class DerivedSlots(DemoSlots): pass
    ds = DerivedSlots()
    ds.att3 = "this is att3"

Méthodes statiques et méthodes de classe
----------------------------------------

Imaginons que l'on veuille compter le nombre d'instances créées par une
classe. Pour récupérer le nombre courant d'instances, on peut écrire une
méthode qui ne se sert que de l'attribut de classe, et cette méthode
devrait pouvoir être appelée via le nom de classe, sans passer par une
instance, mais cela ne fonctionne pas à l'aide d'une implémentation
"naive" telle que celle-ci :

.. code:: python

    class DemoComptage(object):
        __nb_objets = 0
        def __init__(self):
            DemoComptage.__nb_objets = DemoComptage.__nb_objets+1
        def nb_objets():
            return __nb_objets

    a = DemoComptage()
    b = DemoComptage()
    c = DemoComptage()

    print DemoComptage.nb_objets()


::


    ---------------------------------------------------------------------------
    TypeError                                 Traceback (most recent call last)

    <ipython-input-1-225b71655857> in <module>()
         10 c = DemoComptage()
         11
    ---> 12 print DemoComptage.nb_objets()


    TypeError: unbound method nb_objets() must be called with DemoComptage instance as first argument (got nothing instead)


En effet, même si ``self`` n'est pas utilisé dans le corps d'une
méthode, l'interpréteur Python exige qu'une méthode soit invoquée à
travers une instance. L'attribut ``nb_objets`` de la classe est
évidemment accessible à tous, donc il peut êtr lu directement par les
clients, mais si on tient à préserver l'encapsulation, une simple
fonction extérieure peut faire l'affaire :

.. code:: python

    class DemoComptage(object):
        nb_objets = 0
        def __init__(self):
            DemoComptage.nb_objets = DemoComptage.nb_objets+1

    def nb_objets():
        return DemoComptage.nb_objets

    a = DemoComptage()
    b = DemoComptage()
    c = DemoComptage()

    print nb_objets()


.. parsed-literal::

    3


Cependant, pour satisfaire les programmeurs qui tiennent à localiser la
fonction au sein de la classe, depuis Python 2.2, on peut définir des
méthodes dites "statiques", qui peuvent s'invoquer sans passer par une
instance :

.. code:: python

    class DemoComptage(object):
        _nb_objets = 0
        def __init__(self):
            DemoComptage._nb_objets = DemoComptage._nb_objets+1
        def nb_objets():
            return DemoComptage._nb_objets
        nb_objets = staticmethod(nb_objets)

    a = DemoComptage()
    b = DemoComptage()
    c = DemoComptage()

    print DemoComptage.nb_objets()


.. parsed-literal::

    3


Il existe également des méthodes dites "de classe", qui recoivent en
premier argument non pas l'instance courante, mais la classe courante.

.. code:: python

    class DemoClassMethod(object):
        def m(cls,data):
            print cls, data
        m = classmethod(m)

    DemoClassMethod.m("bonjour")


.. parsed-literal::

    <class '__main__.DemoClassMethod'> bonjour


Décorateurs de fonctions
------------------------

Les méthodes statiques et les méthodes de classes sont des cas
particuliers de "décorateurs" de fonction.

Un décorateur est une fonction qui manipule une fonction. Il peut
effectuer une action une fois pour toute, et renvoyer la fonction
originale, ou bien renvoyer une nouvelle fonction qui effectuera des
manipulations à chaque appel, avant de le répercuter à la fonction
originale qui aura été mémorisée en interne.

Par exemple, lors de l'instruction
``nb_objets = staticmethod(nb_objets)``, on appelle le décorateur
``staticmethod``, qui substitue à la fonction ``nb_objets`` originale
une autre fonction, dont le rôle sera, à chaque appel, de mettre de côté
le premier argument (``self``) et de transmettre les autres à la
fonction ``nb_objets`` originale.

On peut maintenant définir plus facilement une décoration, à l'aide du
caractère ``@`` :

.. code:: python

    class DemoDeco(object):
        _nb_objets = 0
        def __init__(self):
            DemoDeco._nb_objets += 1
        @staticmethod
        def nb_objets():
            return DemoDeco._nb_objets
        @classmethod
        def m(cls,data):
            print cls, data

    a = DemoDeco()
    b = DemoDeco()
    c = DemoDeco()

    print DemoDeco.nb_objets()


.. parsed-literal::

    3


.. code:: python

    DemoDeco.m("bonjour")


.. parsed-literal::

    <class '__main__.DemoDeco'> bonjour


On peut empiler autant de décorateurs que souhaités. Le code ci-dessous
:

.. code:: python

    @A @B @C
    def f():
        ...

est l'équivalent de :

.. code:: python

    def f():
        ...
    f = A(B(C(f)))

On peut bien sur écrire ses propres décorateurs. On les implémente en
général à l'aide d'une classe qui stocke la fonction décorée, et définit
l'opérateur d'appel ``__call__``. Par exemple, ci-dessous, une classe
qui compte les appels à la fonction décorée :

.. code:: python

    class compteur(object):
        def __init__(self,func):
            self.func = func
            self.count = 0
        def __call__(self,*args):
            self.count += 1
            print 'call %s to %s' % (self.count,self.func.__name__)
            self.func(*args)

    @compteur
    def bidon(texte):
        print texte

    bidon("bonjour")


.. parsed-literal::

    call 1 to bidon
    bonjour


.. code:: python

    bidon("bonsoir")


.. parsed-literal::

    call 2 to bidon
    bonsoir


Enfin, notons qu'il existe des décorateurs permettant de simplifier la
définition des propriétés d'une classe (attributs dont l'accès en
lecture et/ou en écriture est confié à des méthodes). L'exemple de
vecteur précédemment vu peut-être réécrit ainsi :

.. code:: python

    class Vector(object):
      def init(self,u=0,v=0):
        self.x = u
        self.y = v
      @property
      def x(self):
        return self.__x
      @x.setter
      def x(self,u):
        if (u<-1): self.__x = -1
        elif (u>1): self.__x = 1
        else: self.__x = u
      @property
      def y(self):
        return self.__y
      @y.setter
      def y(self,v):
        if (v<-1): self.__y = -1
        elif (v>1): self.__y = 1
        else: self.__y = v

    v = Vector()
    v.init(3, -4)
    print v.x, v.y


.. parsed-literal::

    1 -1


Hériter d'un type prédéfini
---------------------------

On peut hériter des types prédéfinis, comme de n'importe quelle autre
classe. **ATTENTION**, dans l'exemple ci-dessous, comme on hérite d'une
classe "non-modifiable" ("immutable"), on ne peut pas redéfinir
**``__init__``**, qui intervient après a création de l'instance ; on est
obligé de passer par une redéfinition de **``__new__``**.

.. code:: python

    class FloatWithUnit(float):
        def __new__(cls,value,unit):
            instance = float.__new__(cls,value)
            instance.unit = unit
            return instance
        def __str__(self):
            return float.__str__(self)+" "+self.unit
        def __mul__(self,other):
            return FloatWithUnit(float.__mul__(self,other),self.unit+"*"+other.unit)

    largeur = FloatWithUnit(2,"cm")
    longueur = FloatWithUnit(5,"cm")
    print largeur*longueur


.. parsed-literal::

    10.0 cm*cm


Les classes de style ancien
---------------------------

Toutes les explications de cette formation concerne les classes de
nouveau style, apparue avec 2.2, et qu'il faut utiliser à chaque fois
que c'est possible. Comment savoir si une classe est d'ancien ou nouveau
style ? \* avant 2.2 : toutes les classes sont de stype ancien \* de 2.2
à 2.x : les classes qui ont la classe ``object`` parmi leurs ancêtres
sont de nouveau style. \* à partir de 3 : toutes les classes sont de
nouveau style

Un grand nombre de fonctionnalités avancées ne sont disponibles que pour
les classes de nouveau style : ``super()``, propriétés, encoches,
méthodes statiques, décorateurs, héritage d'un type prédéfini...

Par ailleurs, en cas d'héritage en losange, les comportements son
subtilement différents. Pour les classes de nouveau style, la recherche
d'attribut se fait en profondeur d'abord, puis de gauche à droite, à une
exception près : les classes dérivées sont toujours explorées avant
leurs classes de base. Cette exception n'avait pas cours pour les
classes de style ancien :

.. code:: python

    class A: x = "A"
    class B1(A): pass
    class B2(A): x = "B2"
    class C(B1,B2): pass

    print C.x


.. parsed-literal::

    A


En cas de nouveau style :

.. code:: python

    class A(object): x = "A"
    class B1(A): pass
    class B2(A): x = "B2"
    class C(B1,B2): pass

    print C.x


.. parsed-literal::

    B2


Pour éviter toute ambiguité, quel que soit le style de classe, on peut
dire explicitement quelle est la méthode à utiliser :

.. code:: python

    class A: x = "A"
    class B1(A): pass
    class B2(A): x = "B2"
    class C(B1,B2): x = B2.x

    print C.x


.. parsed-literal::

    B2


Itérateurs
----------

Aujourd'hui, avant de rechercher une méthode ``__getitem__``,
l'interpréteur Python cherche d'abord une méthode ``__iter__``, qui peut
implémenter des schémas d'itération plus complexes. La méthode
``__iter__`` est supposée retourné un objet "itérateur", sur lequel on
va ensuite appeler la méthode ``next`` (en python 3 : ``__next__``), qui
renvoit un nouvel élément à chaque appel, et lève une exception
``StopIteration`` lorsqu'il n'y a plus d'éléments.

.. code:: python

    class ReverseIterator(object):
        def __init__(self, seq):
            self.seq = seq
            self.index = len(seq.data)
        def next(self):
            if self.index == 0:
                raise StopIteration
            self.index = self.index - 1
            return self.seq.data[self.index]

    class Reverse(object):
        def __init__(self, data):
            self.data = data
        def __iter__(self):
            return ReverseIterator(self)

    for char in Reverse('123'):
        print(char)


.. parsed-literal::

    3
    2
    1


Notre classe peut servir elle aussi dans tous les contextes d'itération.

.. code:: python

    inverse = Reverse('123')
    [c for c in inverse]




.. parsed-literal::

    ['3', '2', '1']



.. code:: python

    [c for c in inverse]




.. parsed-literal::

    ['3', '2', '1']



.. code:: python

    [c+d for c in inverse for d in inverse]




.. parsed-literal::

    ['33', '32', '31', '23', '22', '21', '13', '12', '11']



Pour éviter d'avoir à écrire deux classes, on peut être tenté de dire
qu'objet est son propre itérateur, et de placer la méthode ``next()``
dans la classe d'origine (exemple ci-dessous). Dans la pratique, c'est
peu utile, car l'objet ne peut être itéré qu'une seule fois. On peut
alors être tenté d'ajouter une méthode ``raz()`` qui remettrait l'index
à 0, mais avec encore une limite : impossible de lancer deux itérations
simultanées. Ne rusez pas : faites deux classes.

.. code:: python

    class Reverse(object):
        def __init__(self, data):
            self.__data = data
            self.__index = len(data)
        def __iter__(self):
            return self
        def next(self):
            if self.__index == 0:
                raise StopIteration
            self.__index = self.__index - 1
            return self.__data[self.__index]

    inverse = Reverse('123')
    print [c for c in inverse]
    print [c for c in inverse]


.. parsed-literal::

    ['3', '2', '1']
    []


.. code:: python

    class Reverse(object):
        def __init__(self, data):
            self.__data = data
            self.__index = len(data)
        def __iter__(self):
            return self
        def next(self):
            if self.__index == 0:
                raise StopIteration
            self.__index = self.__index - 1
            return self.__data[self.__index]
        def raz(self):
            self.__index = len(self.__data)

    inverse = Reverse('123')
    print [c for c in inverse]
    inverse.raz()
    print [c for c in inverse]
    inverse.raz()
    print [c+d for c in inverse for d in inverse]


.. parsed-literal::

    ['3', '2', '1']
    ['3', '2', '1']
    ['32', '31']
