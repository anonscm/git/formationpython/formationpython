Les modules
===========

-  Un module est un bloc de code réutilisable en python. Ce bloc de code
   peut-être importé par un autre bloc de code. Il existe plusieurs
   types de module : les modules Python, les modules d’extension et les
   packages.
-  Les modules Python sont comme leur nom l’indique écrit en python dans
   un fichier ayant comme extension **.py** (.pyc et/ou .pyo). Ils sont
   parfois appelés «pure module». Le nom du module est le nom du fichier
   (sans l'extension) accessible via la variable globale
   **``__name__``**.
-  Les modules d’extension sont des modules écrits en langage bas-niveau
   utilisable par Python : C/C++ pour Python ou Java pour Jython. Ces
   modules sont généralement contenus dans un seul fichier pré-compilé
   et chargeable dynamiquement comme par exemple un objet partagé
   (fichier .so) sous unix, une DLL sous windows ou une classe java. On
   parle également de modules «built-in», lorsqu’il s’agit de modules de
   la bibliothèque standard (les bibliothèques logicielles de base du
   langage distribuées avec l’interpréteur Python) écrit en C.
-  Un package est un module qui contient d’autres modules. Un package
   est généralement un répertoire contenant un fichier
   **``__init__.py``**.
-  Il existe un package particulier qui est le «root package». C’est la
   racine dans la hiérarchie des paquets. la grande majorité de la
   bibliothèque standard est dans le «root package».

Présentation
------------

Lancer votre éditeur de texte pour créer un fichier python appelé
**fibo.py** dans le répertoire courant et contenant ce bout de code :

.. code:: python

    %%file fibo.py
    #! /usr/bin/env python3
    # -*- coding: utf-8 -*-
    """
    Module nombres de Fibonacci
    """

    # Fibonacci numbers module

    def fib(n):    # write Fibonacci series up to n
        a, b = 0, 1
        while b < n:
            print(b),
            a, b = b, a + b

    def fib2(n): # returns Fibonacci series up to n
        result = []
        a, b = 0, 1
        while b < n:
            result.append(b)
            a, b = b, a + b
        return result


Une fois ce fichier créé, lancer l’interpréteur via la commande python
et importer le module créé :

.. code:: shell

    >>> import fibo

Une fois le module importé, vous pouvez accéder aux méthodes et
variables via son nom.

.. code:: shell

    >>> fibo.fib(1000)
    1 1 2 3 5 8 13 21 34 55 89 144 233 377 610 987
    >>> fibo.fib2(100)
    [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
    >>> fibo.__name__
    'fibo'

-  Un module peut contenir aussi bien des déclarations (*statements*)
   que des définitions de fonctions.
-  Les déclarations sont destinées à initialiser le module.
-  Elles sont exécutées seulement lors du premier *import* du module
   (pour forcer le rechargement, il faut utiliser *reload()*)
-  Chaque module possède son propre espace de nommage (ou table de
   nommage) «privé» qui est global pour toutes les fonctions de ce même
   module. Les variables sont accessibles via la notation
   «nommodule.nomvariable» (comme pour une fonction)
-  Un module peut importer d’autres modules. Par convention, on place
   tous les imports au début du module.
-  L’instruction «import» crée un objet de type module contenant toutes
   les données et fonctions du module, et ajoute une variable désignant
   ce module au sein de l’espace de nommage du
   programme/module/interpéteur qui fait l’import. Il existe une
   variante qui permet d’importer directement dans la table de nommage
   un ou des éléments particuliers du module : **from module import
   nom**
-  Par exemple :

   .. code:: shell

       >>> from fibo import fib, fib2
       >>> fib(500)
       1 1 2 3 5 8 13 21 34 55 89 144 233 377

   *Dans cet exemple, fib et fib2 sont ajoutés dans la table de nommage
   locale. Fibo n’est pas défini. On peut aussi utiliser le symbole \*
   qui va importer toute la table de nommage sauf ceux qui contiennent
   un tiret-bas* *(underscore : \_ )*

Exécution d’un module
---------------------

Pour exécuter un module Python, on utilise cette commande :

.. code:: shell

    $> python fibo.py <arguments\>

Le code va être exécuté comme lors d’un import mais la valeur de la
variable **name** est alors égale à **main**. En ajoutant ce bout de
code à la fin de votre module :

.. code:: python

    if __name__ == "__main__":
        import sys
        fib(int(sys.argv[1]))

vous rendrez ce fichier utilisable comme un script ou comme un module.

.. code:: shell

    $ python fibo.py 50
    1 1 2 3 5 8 13 21 34

Pour rappel, si le module est importé, le code du *main* ne sera pas
exécuté :

.. code:: python

    >>> import fibo
    >>>

Enfin, il est possible d’exécuter un module python comme un script Unix
en ajoutant :

.. code:: python

    #! /usr/bin/env python3

en début de fichier. Le module sera alors exécutable (à condition que
les droits sur le fichier le permette après un *chmod u+x ./fibo.py* par
exemple) via la commande

.. code:: shell

    $> ./fibo.py <arguments>

Le «path»
---------

Quand un module nommé **spam** est importé, l’interpreteur cherche en
premier ce nom dans les modules intégrés. S’il ne le trouve pas, il va
ensuite chercher un fichier nommé **spam.py** d’une liste de répertoires
donnée par la variable **sys.path**.

.. code:: python

    >>> import sys
    >>> sys.path
    ['', '/usr/lib/python2.7', '/usr/lib/python2.7/plat-linux2', '/usr/lib/python2.7/lib-tk', '/usr/lib/python2.7/lib-old', '/usr/lib/python2.7/lib-dynload', '/usr/local/lib/python2.7/dist-packages', '/usr/lib/python2.7/dist-packages', '/usr/lib/python2.7/dist-packages/PIL', '/usr/lib/pymodules/python2.7']

**sys.path** contient le répertoire courant, le PYTHONPATH et les
dépendances par défaut de l’installation python.

Les modules Compilés
--------------------

-  Pour accélerer le chargement des modules, une version compilée (ou
   *pré-compilée*) du module est présente dans le même répertoire. La
   compilation est faite automatiquement par Python si le module est
   plus récent que l’éventuelle compilation disponible. Cette dernière
   possède l’extension *.pyc*.
-  la compilation n’augmente pas la vitesse d’exécution mais la vitesse
   de chargement du module.
-  Il ne s’agit que d’une version convertie en «byte code» du module qui
   est donc indépendante de la plateforme d’exécution. Les modules
   *.pyc* peuvent donc être partagés (on parle de portabilité).

Les modules standards
---------------------

-  Python fournit toute une bibliothèque de modules standards décrite
   dans un document séparé : la *Python Library Reference*.
-  Certains modules sont intégrés à l’interpréteur (selon par exemple le
   système d’exploitation : winreg est fourni uniquement sur les
   systèmes Windows) et permettent un accès aux opérations qui ne font
   pas partie du noyau de la langue (**dir** ou **help** que nous allons
   voir après par exemple) pour améliorer l’efficacité ou un accès à des
   primitives du système d’exploitation (**sys** qui est fourni dans
   chaque interpréteur Python).

.. code:: python

    >>> import sys
    >>> sys.ps1
    '>>> '
    >>> sys.path
    ['', '/usr/lib/python2.7', '/usr/lib/python2.7/plat-linux2', '/usr/lib/python2.7/lib-tk', '/usr/lib/python2.7/lib-old', '/usr/lib/python2.7/lib-dynload', '/usr/local/lib/python2.7/dist-packages', '/usr/lib/python2.7/dist-packages', '/usr/lib/python2.7/dist-packages/PIL', '/usr/lib/pymodules/python2.7']

Vous pouvez faire un ***sys.path.append*** pour ajouter un répertoire de
développement utilisable par différents modules.

La fonction dir()
-----------------

La fonction intégrée dir() renvoie la liste des noms définis par un
module.

.. code:: python

    >>> import fibo
    >>> dir(fibo)
    ['__builtins__', '__doc__', '__file__', '__name__', '__package__', 'fib', 'fib2']

Sans argument, la fonction **dir()** liste les noms que l’on a
actuellement définis :

.. code:: python

    >>> import fibo
    >>> dir(fibo)
    ['__builtins__', '__doc__', '__file__', '__name__', '__package__', 'fib', 'fib2']
    >>> a = [1, 2, 3, 4, 5]
    >>> import sys
    >>> dir()
    ['__builtins__', '__doc__', '__name__', '__package__', 'a', 'fibo', 'sys']

-  **``__builtins__``** est l’espace de noms contenant toutes les
   fonctions et variables intégrées
-  **``__doc__``** contient l’aide du module
-  **``__file__``** contient le nom du fichier Python contenant le
   module
-  **``__name__``** contient le nom du module
-  **``__package__``** contient le nom du package contenant le module

.. code:: python

    >>> fibo.__doc__
    '\nModule nombres de Fibonacci\n'
    >>> print fibo
    <module 'fibo' from 'fibo.py'>
    >>> fibo.__file__
    'fibo.py'
    >>> fibo.__name__
    'fibo'
    >>> fibo.__package__
    >>>

La fonction help()
------------------

-  Aide en ligne Python.
-  la commande **help()** place l’interpréteur en mode d’aide

.. code:: shell

    >>> help()

    Welcome to Python 2.7!  This is the online help utility.

    If this is your first time using Python, you should definitely check out
    the tutorial on the Internet at http://docs.python.org/2.7/tutorial/.

    Enter the name of any module, keyword, or topic to get help on writing
    Python programs and using Python modules.  To quit this help utility and
    return to the interpreter, just type "quit".

    To get a list of available modules, keywords, or topics, type "modules",
    "keywords", or "topics".  Each module also comes with a one-line summary
    of what it does; to list the modules whose summaries contain a given word
    such as "spam", type "modules spam".

    help> figo
    no Python documentation found for 'figo'

-  **help(module)** renvoie l’aide du module

.. code:: python

    >>> import fibo
    >>> help(fibo)
    Help on module fibo:

    NAME
        fibo - # Fibonacci numbers module

    FILE
        /root/fibo.py

    FUNCTIONS
        fib(n)

        fib2(n)

-  **help(module.methode)** renvoie l’aide de la méthode

--------------

\*\* Ajoutons de l’aide à notre module fibo : \*\*

.. code:: python

    %%file fibo_help.py
    #! /usr/bin/env python3
    # -*- coding: utf-8 -*-

    """
    Fibonacci numbers module
    """

    def fib(n):    # write Fibonacci series up to n
        """
        do : write Fibonacci series up to n
        parameters : n as number
        return : void
        """
        a, b = 0, 1
        while b < n:
            print b,
            a, b = b, a + b

    def fib2(n):   # returns Fibonacci series up to n
        result = []
        a, b = 0, 1
        while b < n:
            result.append(b)
            a, b = b, a + b
        return result

\*\* Voici le résultat :\*\*

.. code:: python

    >>> reload(fibo)
    <module 'fibo' from 'fibo.py'>
    >>> help(fibo)
    Help on module fibo:

    NAME
        fibo - Fibonacci numbers module

    FILE
        /root/fibo.py

    FUNCTIONS
        fib(n)
            do : writes Fibonacci series up to n
            parameters : n as number
            return : void

        fib2(n)

Les packages
------------

-  Les packages sont une façon de structurer l’espace de noms des
   modules Python en utilisant le «dotted module names» (nom de module
   avec des points). Par exemple, le module nommé **A.B** désigne un
   module nommé **B** dans le package nommé **A**.
-  Si on prend l’exemple d’un projet de traitement de fichier texte, il
   existe un grand nombre de formats (.txt, .xml, .csv, .json, *etc*) et
   vous pouvez avoir un grand nombre d’opération de traitement à
   effectuer (compression, exportation, *etc*)
-  Voici un exemple de structure possible pour ce projet :

.. code:: python

    traitement/                          Racine du package
          __init__.py                    Initialise le package traitement
          formats/                       Subpackage pour les formats de fichiers
                  __init__.py
                  csv.py
                  json.py
                  txt.py
                  xml.py
                  …
          compression/                   Subpackage pour la compression
                  __init__.py
                  zip.py
                  tar.py
                  …
          exportation/                   Subpackage pour l’exportation
                  __init__.py
                  pdf.py
                  html.py
                  …

-  Lors de l’importation d’un package, Python cherche dans les
   répertoires du **sys.path** à la recherche du sous-répertoire du
   paquet.
-  Dans l’exemple ci-après, lors de l’instruction
   ``import calculatrice``, Python trouve le sous-répertoire
   **calculatrice** dans le répertoire
   **/Library/Python/2.7/site-packages** contenu dans le **sys.path**

::

        >>> import sys
        >>> sys.path
        ['', ..., '/Library/Python/2.7/site-packages']
        >>> import calculatrice
        >>> calculatrice.__file__
        '/Library/Python/2.7/site-packages/calculatrice/__init__.pyc'

-  Les fichiers **``__init__.py``** permettent d’indiquer que le
   répertoire est un package. Ils permettent d’exécuter du code lors de
   l’initialisation (instancier la variable **``__all__``** par
   exemple).
-  On peut importer un module du package ci-dessus avec cette commande :

.. code:: python

    import traitement.formats.csv

-  Cette commande charge le module **traitement.formats.csv**. Pour
   l’utiliser, il devra être référencé par son nom complet tel
   qu’importé :

.. code:: python

    >>> traitement.formats.csv.read('file1.csv')

-  Une autre façon d’importer un «sous-module» est :

.. code:: python

    from traitement.formats import csv

-  Cette commande charge le module **csv** et le rend disponible sans le
   préfixe du package. De cette manière, la fonction ``read`` est
   utilisable de cette manière :

.. code:: python

    >>> csv.read('file1.csv')

-  Enfin, si on a besoin seulement de la fonction ``read``, on peut la
   charger directement :

.. code:: python

    from traitement.formats.csv import read

-  Cette dernière fonction charge quand même le module **csv** mais rend
   la fonction ``read`` directement disponible :

.. code:: python

    >>> read('file1.csv')

--------------

-  Attention : quand vous utilisez la commande
   ``from package import item``, l’\ **item** peut être un module (ou un
   sous-package) du package mais également un autre nom défini dans un
   module comme une variable, une fonction ou une classe.
-  L’instruction ``import`` teste en premier si l’élément est défini
   dans le paquet sinon il suppose que c’est un module et tente de le
   charger. Si le module n’est pas trouvé, l’exception **ImportError**
   est levée.
-  Si vous utilisez par contre la syntaxe
   ``import item.subitem.subsubitem``, chaque item excepté le dernier
   doit être un package. Le dernier peut être un module ou un package
   mais ne peut être une variable, une fonction ou une classe.
-  Lorsque l’on importe **paquet.module** pour la première fois, cela
   crée l’objet paquet correspondant (et son espace de noms) et exécute
   le fichier ``__init__.py`` correspondant. Ce dernier est exécuté
   seulement la première fois. Le fichier ``__init__.py`` peut créer des
   données et des fonctions au sein de l’objet correspondant au package,
   c’est pourquoi lors de l’exécution de l’instruction
   ``import paquet.nom``, Python cherche d’abord une variable ou
   fonction dans l’objet correspondant au paquet, avant de chercher un
   module ou un paquet dans le répertoire du paquet.

Complément d’information
------------------------

-  L’instruction ``from package import \*`` est déconseillée. Cette
   instruction va lancer le parcours des modules du package et faire
   toutes les importations. Cela peut prendre donc un certain temps et
   l’importation des modules peut générer des conflits dans la table de
   nommage. L’auteur du package peut fournir une liste explicite des
   modules à importer grâce à la variable ``__all__`` dans le fichier
   ``__init__.py`` du package.

.. code:: python

    __all__ = ["csv", "json", "txt", "xml"]

-  Enfin, pour les import «intra-package», on peut utiliser les
   références relatives.
-  Par exemple, depuis le module **zip**, on peut utiliser ces commandes
   :

.. code:: python

    from . import tar
    from .. import formats
    from ..exportation import pdf

Distribuez votre projet : le packaging
--------------------------------------

-  Une distribution est une collection de modules Python distribués
   ensembles comme une ressource téléchargeable et destinée à être
   installée. Il existe énormément de distributions de modules comme
   *PIL (the Python Imaging Library)*, *PyXML*, *etc*.

-  Pour faire le lien entre la distribution et la plateforme de
   destination, on utilise des classes intermédiaires appelées
   *packagers*. Les packagers vont prendre les sources et les compiler
   pour effectuer une «release». Ainsi les utilisateurs finaux vont
   pouvoir installer les modules sans difficultés. Distutils est un
   packagers qui va vous permettre facilement de distribuer votre code.

-  La librairie Distutils regroupe l’ensemble des utilitaires python
   pour la distribution de modules. Pour distribuer votre code, il
   faudra écrire un script d’installation (nommé setup.py par
   convention) et éventuellement écrire un fichier de configuration
   d’installation.
-  Ensuite, il vous faudra créer une ressource distribuable (souvent une
   archive) et optionnellement créer une ou plusieurs distributions
   compilées.

-  Le script setup est généralement assez simple. Voici un premier
   exemple :

.. code:: python

    from distutils.core import setup
    setup(name='foo',
          version='1.0',
          py_modules=['foo'],
          )

-  La plupart des informations sont fournies comme argument à la
   fonction setup.
-  Ces arguments peuvent être regroupés en deux catégories : les
   metadata du package (nom, version) et les informations sur ce qu’est,
   ce que fait le package.
-  Les modules sont spécifiés par leur nom d’objet et non leur nom de
   fichier. Il est recommandé de fournir des metadata supplémentaire
   comme son nom, son adresse mail et une url de projet :

   -  Vous pouvez lister des modules individuellement : py\_modules =
      ['mod1', 'pkg.mod2']
   -  ou lister des packages entier : packages=['distutils',
      'distutils.command'] Ici on spécifie des modules pur python par
      package plutôt que de lister tous les modules de ce paquet.
   -  par exemple ce package :

      .. code:: python

          setup.py
          src/
              mypkg/
                  __init__.py
                  module.py
                  data/
                      tables.dat
                      spoons.dat
                      forks.dat

   -  pourrait avoir un setup comme cela :

      .. code:: python

          setup(...,
            packages=['mypkg'],
            package_dir={'mypkg': 'src/mypkg'},
            package_data={'mypkg': ['data/*.dat']},
            )

-  Voici une liste non exhaustive d'argument de la fonction **setup**

   -  le nom du projet : nom="sample"
   -  la version : version="1.2.0" (voir PEP440)
   -  les packages à inclure dans le projet. On peut les lister ou
      utiliser find\_packages pour automatiser cette tache (exclude pour
      pour exlure ceux qui ne doivent pas etre installé) :
      py\_modules=['foo'],
   -  Metadonnées: Il est important d'inclure des métadonnées à propose
      de votre projet.

      .. code:: python


              # A description of your project
              description='A sample Python project',
              long_description=long_description,

              # The project's main homepage
              url='https://github.com/pypa/sampleproject',

              # Author details
              author='The Python Packaging Authority',
              author_email='pypa-dev@googlegroups.com',

              # Choose your license
              license='MIT',

              # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
              classifiers=[
                  # How mature is this project? Common values are
                  #   3 - Alpha
                  #   4 - Beta
                  #   5 - Production/Stable
                  'Development Status :: 3 - Alpha',

                  # Indicate who your project is intended for
                  'Intended Audience :: Developers',
                  'Topic :: Software Development :: Build Tools',

                  # Pick your license as you wish (should match "license" above)
                  'License :: OSI Approved :: MIT License',

                  # Specify the Python versions you support here. In particular, ensure
                  # that you indicate whether you support Python 2, Python 3 or both.
                  'Programming Language :: Python :: 2',
                  'Programming Language :: Python :: 2.6',
                  'Programming Language :: Python :: 2.7',
                  'Programming Language :: Python :: 3',
                  'Programming Language :: Python :: 3.2',
                  'Programming Language :: Python :: 3.3',
                  'Programming Language :: Python :: 3.4',
              ],

              # What does your project relate to?
              keywords='sample setuptools development',

-  Enfin, il est possible d'ajouter :

   -  Les dépendances : install\_requires = ['peppercorn']
      "install\_requires" est utilisé pour spécifier quelles dépendances
      sont nécessaire au projet pour fonctionner. Ces dépendances seront
      installés par Pip lors de l'installation de votre projet.
   -  Fichiers additionnels : package\_data = { 'sample':
      ['package\_data.dat']}

-  Voici un exemple plus complet :

   .. code:: python

       #!/usr/bin/env python

       from distutils.core import setup

       setup(name='Distutils',
             version='1.0',
             description='Python Distribution Utilities',
             author='Greg Ward',
             author_email='gward@python.net',
             url='https://www.python.org/sigs/distutils-sig/',
             packages=['distutils', 'distutils.command'],
            )

-  Vous pouvez également inclure d'autre fichier au package comme un
   ***README*** pour expliquer le projet et un ***MANIFEST*** pour
   définir des fichiers supplémentaire à inclure dans la distribution du
   projet packaté.

-  Pour créer une distribution des fichiers sources du module, il faut
   donc créer un script d’installation (setup.py) contenant le code
   ci-dessus et d’executer la commande

   ::

       $>python setup.py sdist [$>sdist setup.py sous windows]

-  sdist va créer une archive (tarball sous unix et zip sous windows)
   contenant le script setup.py et le module. Le fichier d’archive sera
   nommé foo-1.0.tar.gz (ou .zip) et sera décompressé dans un répertoire
   foo-1.0.

-  Pour installer le module, après avoir télécharger et décompresser
   l'archive, il faut se déplacer dans le répertoire créer par la
   décompression de l'archive et taper la commande suivante :

   ::

       $>python setup.py install

-  Cette commande va copier les fichiers dans le répertoire réservé aux
   modules tiers de l’installation Python.
-  Remarque : On note donc que c'est le même script qui sert pour la
   distribution et l’installation.

-  On peut faciliter encore plus l’installation des modules distribués.
   Par exemple, sous windows, on peut créer un installateur executable
   avec cette commande :

   ::

       $>python setup.py bdist_wininst

-  Cette commande var créer un exe nommé foo-1.0.win32.exe dans le
   répertoire courant.
-  Il existe également d’autre format de distribution : le rpm avec
   bdist\_rpm, le pkgtool (bdist\_pkgtool) et le hp\_ux swinstall
   (bdist\_sdux)
-  Vous pouvez lister les formats de distribution disponibles avec cette
   commande :

   ::

       $>python setup.py bdist –help-formats

Pour installer des packages, on vous conseille d'utiliser Pip (Python
installing package). Cet outil cherche les packages sur le Python
Package Index (PyPi). Les packages python peuvent être compacté dans des
archives tarball ou zip. Python utilise des formats de distribution.
Actuellement, Python utilise egg mais ce format va etre peu à peu
remplacé par wheel. Pour construire votre package, vous pouvez donc
utiliser **wheel** et pour envoyer votre package sur Pypi, il faut
utiliser l'outil **twine**
