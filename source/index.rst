.. Formation Python 3 documentation master file, created by
   sphinx-quickstart on Wed Nov  4 23:08:12 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Formation Python 3
==================

Il s'agit d'une formation de base à Python. La même formation sera jouée
dans les différentes régions, par des intervenants locaux.

6 demi-journées:

-  Introduction : installation et prise en main, initiation à la syntaxe
   Python, les types de données standard, les structures de contrôle.
-  Les fonctions                                                                
-  Les modules et le packaging                                                  
-  La programmation objet                                                       
-  Les tests et la documentation                                                
-  Projet de manipulation                                                       
                                                                                
Les travaux pratiques de la formation seront effectués avec                     
l'environnement virtualenv et IPython (Python 3).  

Les supports de cours sont disponibles sur Sourcesup:

`https://sourcesup.renater.fr/frs/?group_id=2055 <https://sourcesup.renater.fr/frs/?group_id=2055>`_

Vous pouvez également accèder aux sources de cette formation avec::

  git clone https://git.renater.fr/formationpython.git



Sommaire
--------

.. toctree::
   :numbered:

   introduction
   fonctions
   modules
   objets
   tests
   projet
   docs

