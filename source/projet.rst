===============
Projet BallPack
===============

Vous allez à présent écrire un projet de A à Z en utilisant tout ce que
vous avez vu pendant ces 5 demi journées.

Le projet proposé est l'écriture d'un package permettant de représenter
des balles qui se déplacent dans un carré et qui rebondissent sur les
bords. On utilisera *Tkinter* pour l'interface graphique car il est
fourni de base avec Python mais il y a aujourd'hui beaucoup mieux

-  `PySide <http://qt-project.org/wiki/PySide>`__
-  `PyQt <http://www.riverbankcomputing.co.uk/software/pyqt/intro>`__
-  `Kivy <http://kivy.org/#home>`__

Les balles pourront se déplacer de différentes manières:

- à une vitesse constante,
- avec une force de gravité,
- avec une fonction proposée par l'utilisateur.

L'arborescence du projet final devra ressembler à

.. image:: _static/tree.png


Voici également une vue de l'interface graphique

.. image:: _static/ballpack.png


Nous allons commencer par mettre en place le virtualenv, ensuite on
implémentera les balles et enfin on créera la documentation du projet.

Récuperez ``ballPack_squelette`` pour commencer ce projet.

D'autre part, il faudra avoir les librairies Tk installées au niveau de votre
système (sous ubuntu: **sudo apt-get install python3-tk**).

Création du virtualenv
======================

Initialisation
--------------

Créer un répertoire **projet** dans lequel vous mettrez le projet (dans le
répertoire **ballPack** à partir du squelette=)
ainsi que le virtualenv (dans le répertoire **env**).

Entrer dans le virtualenv pour la suite des questions.

Installation des dépendances
----------------------------

Notre projet dépend de plusieurs paquets (ipython, sphinx, tikitiki),
installez-les dans le virtualenv.

Dans un virtualenv, vous pouvez obtenir
la liste des dépendances installées.

.. parsed-literal::

    pip freeze


.. literalinclude:: ../projet/requirements.txt
    :linenos:


Du coup, vous pouvez facilement le sauvegarder dans un fichier:

.. parsed-literal::

    pip freeze > requirements.txt


Et vous en servir pour mettre à jour votre virtualenv:

.. parsed-literal::

    pip install --upgrade --requirement requirements.txt


Le module ``ballMod.py``
========================

Une balle, qu'elle soit à vitesse constante, avec une force de gravité,
..., a toujours les mêmes attributs.

- ``coords``: liste des coordonnées du centre de la balle
- ``v``: vecteur vitesse de la balle
- ``radius``: rayon de la balle
- ``domain``: liste de liste comprenant les dimensions du domaine
  :math:`[[x_{min}, x_{max}], [y_{min}, y_{max}]]`
- ``color``: la couleur de la balle

Question 1
----------

Ecrivez une classe ``Ball`` qui sera la classe mère. Voici la
documentation de sa méthode ``__init__``

::

    """Initialisation de la classe Ball

    :param x: coordonnée du centre de la balle suivant x
    :param y: coordonnée du centre de la balle suivant y
    :param vx: vitesse de la balle suivant x (défaut: 1)
    :param vy: vitesse de la balle suivant y (défaut: 1)
    :param radius: rayon de la balle (défaut: 20)
    :param xdomain: domaine de la balle suivant x (défaut: [0, 800])
    :param ydomain: domaine de la balle suivant y (défaut: [0, 600])
    :param color: couleur de la balle (défaut: "green")
    """

Déduisez en son implantation.

Question 2
----------

Ecrivez une méthode ``check_velocity`` qui ne prend pas de paramètre et
qui vérifie si la balle ne sort pas du domaine pour chacune des
directions.

Si elle sort, la méthode

- remet le centre de la balle à l'intérieur (on prendra les bords du
   domaine plus ou moins le rayon),
- inverse le sens de la vitesse concernée,
- renvoie ``True``.

Sinon, elle renvoie ``False``.

Question 3
-----------

Ecrivez une méthode qui permet d'avoir l'affichage suivant lorsque l'on
fait un ``print`` d'une balle

::

    Ball:
         coord    : [1, 1]
         velocity : [1, 1]
         radius   : 20
         color    : green
         domain   : [[0, 800], [0, 600]]

Nous allons maintenant créer trois classes filles. Pour chacune d'elles,
nous allons implanter la méthode ``move`` permettant de déplacer la
balle au cours du temps.

Question 4
----------

Ecrivez une classe ``BouncingBall`` qui hérite de la classe ``Ball`` et
qui fait juste rebondir la balle sur les bords. Sa méthode ``move`` est
très simple

-  on change les coordonnées de la manière suivante

.. math::


   \begin{array}{l}
   x = x + v_x \\
   y = y + v_y
   \end{array}

-  on regarde si les coordonnées ne sortent pas du domaine et on les
   modifie si nécessaire.

Question 5
----------

Ecrivez une classe ``GravityBall`` qui hérite de la classe ``Ball`` et
où la vitesse :math:`y` est soumise à la gravité. De plus, dès que la
balle touche un bord, elle est amortie. Elle a besoin de différents
paramètres que vous pourrez mettre directement dans la méthode

- :math:`\Delta t = 0.1`
- :math:`g = 9.81`
- :math:`c_a = 0.9`

Sa méhode ``move`` est

-  on change les coordonnées de la manière suivante

.. math::


   \begin{array}{l}
   v_y = v_y - \Delta t*g \\
   x = x + \Delta t*v_x \\
   y = y - \Delta t*v_y
   \end{array}

-  on regarde si les coordonnées ne sortent pas du domaine et on les
   modifie si nécessaire.

-  si on est sorti du domaine, on amortie les vitesses

.. math::


   \begin{array}{l}
   v_x = c_a*v_x \\
   v_y = c_a*v_y
   \end{array}

Question 6
----------

Ecrivez le fichier ``setup.py`` permettant d'installer le package
``ballPack`` avec le module ``ballMod``.

Question 7
-----------

Ecrivez des tests sur les classes précédemment créées.

Le module ``color.py``
======================

Dans ce module, nous allons créer une classe ``Color`` qui prend en
paramètre un fichier contenant des noms de couleurs et qui les stocke
dans un attribut ``colorNames`` qui est de type ``list``.

Question 1
-----------

Ecrivez la méthode ``___init__`` dont la documentation est

::

        """
        :param filename: nom du fichier contenant les couleurs (défaut: None)
        Si None, on lit le fichier 'data/rgb.txt' se trouvant dans ballPack.
        """

On utilisera la méthode ``os.path.dirname`` pour retrouver le chemin du
fichier ``data/rgb.txt`` une fois le module installé.

Question 2
----------

Ecrivez une méthode ``get_random_color`` qui renvoie de manière aléatoire
une couleur de la liste ``colorNames``. On utilisera la méthode
``choice`` du module ``random``.

Question 3
----------

Modifiez le ``setup.py`` pour avoir ce module et le fichier
``data/rgb.txt`` installé.

Le script ``tkgui.py``
======================

Question 1
-----------

Modifiez le fichier ``setup.py`` pour que le script ``script/tkgui.py``
soit installé.

Question 2
-----------

Exécutez ce script et corrigez votre package pour que celui-ci
fonctionne.

Le module ``save.py``
=====================

Nous allons à présent faire en sorte de pouvoir sauvegarder et recharger
un état au format CSV. On se servira du module
```csv`` <https://docs.python.org/3/library/csv.html>`__ qui est de base
dans Python.

Question 1
-----------

Ecrivez une fonction ``save_balls`` dont la documentation est

::

        """
        sauvegarde d'une liste de balles au format csv.

        :param balls: liste à sauvegarder
        :param filename: fichier de la sauvegarde
        :param delimiter: délimiteur entre chaque entrées (défaut: ',')
        """

Question 2
-----------

Ecrivez une fonction ``read_balls`` dont la documentation est

::

        """
        lit un fichier csv contenant une liste de balles et
        renvoie cette liste en les contruisant.

        :param filename: fichier de la sauvegarde
        :param delimiter: délimiteur entre chaque entrées (défaut: ',')
        :return: liste de balles
        """

Question 3
-----------

Décommentez les lignes de la méthode ``saveAndLoadState`` dans
``tkgui.py``, réinstallez le package et testez.

Ajout d'une classe ``UserBall``
===============================

Ajoutez une classe ``UserBall`` qui permet de déplacer une balle selon
une fonction et ses paramètres donnés en paramètre de la fonction
``move``. Puis testez.

Documentation du projet
=======================

Il est possible de créer de la documentation à partir des commentaires
dans le code de notre projet.

.. parsed-literal::

    pip install Sphinx
    sphinx-quickstart


Il faudra s'assurer de bien activer l'extension **autodoc**. Une fois la
structure de base créée, vous pourrez créer la documentation en html avec:

.. parsed-literal::

    make html
