Documentations
==============

Voici quelques liens utiles:

- `PEP 8 <http://www.python.org/dev/peps/pep-0008/>`_
- `Python <https://docs.python.org/3/>`_
- `Sphinx <http://sphinx-doc.org/>`_
