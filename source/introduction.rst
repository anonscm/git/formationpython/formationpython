============
Introduction
============

Qu'est ce que Python ?
======================

Python a été créé par **Guido van Rossum** en 1989.

**Le langage Python :**

-  est placé sous **une licence libre** proche de la licence BSD2
-  **fonctionne sur la plupart des plates-formes informatiques**, des
   supercalculateurs aux ordinateurs centraux, de Windows à Unix en
   passant par GNU/Linux, Mac OS, ou encore Android, iOS, et aussi avec
   Java ou encore .NET.

Il est conçu pour optimiser la productivité des programmeurs en offrant
des outils de haut niveau et une syntaxe simple à utiliser.

.. image:: _static/python-logo-TM.png


Que peut-on faire avec Python ?
-------------------------------

**Web**

::

    Django, TurboGears, Zope, Plone, ...

**Bases de données**

::

    MySQL, PostgrSQL, Oracle, ...

**Réseaux**

::

    TwistedMatrix, PyRO, ...

**UI**

::

    Gtk, Qt, Tcl/Tk, WxWidgets, Kivy,...

**Représentation graphique**

::

    gnuplot, matplotlib, VTK, ...

**Calcul scientifique**

::

    numpy, scipy, sage, ...

...


Principales caractéristiques de ce langage
------------------------------------------

Python est un langage de programmation qui permet de travailler plus
vite.

- langage de programmation à usage général de haut niveau
- riche et polyvalent
- interprété
- ouvert et multiplateforme
- facilité d'apprentissage et de mise en œuvre
- diversité des outils de développement
- stable/mature, très répandu et évolutif

Comment est traduit mon code en langage machine ?
-------------------------------------------------

Il existe 2 techniques principales pour effectuer la traduction en
langage machine de mon code source :

-  Interprétation

.. image:: _static/Interpretation.png

-  Compilation

.. image:: _static/Compilation.png


Comment est traduit mon code en langage machine ?
-------------------------------------------------

Qu'en est-il pour mon code Python ?

.. image:: _static/TraductionPython.png


Les différentes versions de Python
----------------------------------

Il existe deux versions de Python (Python 2.x et Python 3.x).

Les modifications apportées sont assez importantes pour qu'un code écrit
en 2.x ne fonctionne pas en 3.x. Pour vous aider à transférer vos codes,
vous avez à disposition un programme
`2to3.py <https://docs.python.org/2/library/2to3.html>`__.

Nous avons choisi de réaliser cette formation sur la version 3 et en
utilisant l'environnement virtualenv. Le choix de la version est
important puisqu'il est en étroite relation avec les modules et paquets
dont vous avez besoin.

Les implémentations de Python
-----------------------------

Il existe plusieurs implémentations de Python qui permettent d'étendre
le langage avec des librairies dans d'autres langages :

-  **CPython** : L'interpréteur de référence de Python. Il génère du
   byte-code Python (fichier \*.pyc) et est écrit en C; il permet
   d'étendre le langage avec des librairies C.
-  **Jython** : Interpréteur qui permet de coupler du Python et du Java
   dans le même programme, génère du byte-code Java.
-  **IronPython** : Implémentation de Python qui vise .Net et Mono,
   permet de coupler Python avec le framework .Net.
-  **Pypy** : Implémentation de Python en Python; projet de recherche
   pour obtenir une implémentation plus rapide que l'implémentation de
   référence (CPython).

Plus d'informations sur les implémentations
`ici <https://wiki.python.org/moin/PythonImplementations>`__

Les différents IDE pour Python
------------------------------

+-------------------------------------------------------------------------+------------------------------------------------------+
| IDE multi-plateforme                                                                                                           |
+=========================================================================+======================================================+
| `NetBeans <http://wiki.netbeans.org/Python>`__                          | `PyCharm <https://www.jetbrains.com/pycharm/>`__     |
+-------------------------------------------------------------------------+------------------------------------------------------+
| `KDevelop <https://www.kdevelop.org>`__                                 | `PyDev <http://pydev.org>`__                         |
+-------------------------------------------------------------------------+------------------------------------------------------+
| `Wing IDE <http://wingware.com>`__ - propriétaire                       | `Spyder <https://pythonhosted.org/spyder/>`__        |
+-------------------------------------------------------------------------+------------------------------------------------------+
| `IDLE <https://docs.python.org/2/library/idle.html>`__                  | `IDLEX <http://idlex.sourceforge.net>`__             |
+-------------------------------------------------------------------------+------------------------------------------------------+
| `IEP <http://www.iep-project.org>`__                                    | `PTK <http://pythontoolkit.sourceforge.net>`__       |
+-------------------------------------------------------------------------+------------------------------------------------------+
| `PyStudio <https://code.google.com/p/editra-plugins/wiki/PyStudio>`__   | `Eric IDE <http://eric-ide.python-projects.org>`__   |
+-------------------------------------------------------------------------+------------------------------------------------------+

Plus d'informations sur les IDE
`ici <https://wiki.python.org/moin/IntegratedDevelopmentEnvironments>`__

L'environnement virtualenv
==========================

Prérequis
---------

Pour la suite, vous aurez besoin de la commande **pyvenv-3.4** (ou **pyvenv**
selon les distributions). 

Sous Ubuntu, il faudra installer un paquet::

  sudo apt-get install python3-virtualenv


Description
-----------

Virtualenv est un outil qui permet de créer des environnements Python
isolés. Cela permet de travailler sur plusieurs projets ayant chacun ses
propres dépendances en évitant notamment les conflits entre différentes
versions d'une même bibliothèque.

Utilisation
-----------

-  pour créer un environnement virtuel (virtualenv) nommé *env* :
   **pyvenv-3.4 env**
-  pour rentrer dans le virtualenv : **source env/bin/activate**
-  pour installer ipython dans le virtualenv: **pip install ipython**
-  pour en sortir : **deactivate**
-  pour le supprimer : **rm -rf env**

Premiers pas avec l'interpréteur interactif
-------------------------------------------

Ce matin, le cours va se poursuivre sous
`IPython <http://ipython.org>`__.

IPython est un interpréteur Python à la différence qu'il permet de :

-  chercher une méthode dans un module ou un namespace,
-  afficher le prototype d'une méthode ou d'une fonction,
-  utiliser la complétion dans l'espace de nom local et
-  afficher l'historique des commandes.

Dans IPython, on accède à l'aide en tapant **help()** et on quitte
l'aide avec la commande **quit**.

L'aide dans IPython, c'est aussi :

-  **help("topics")** : énumère les différents "concepts" Python au
   sujet desquels on peut demander de l'aide ; taper ensuite
-  **help("SUJET")** pour avoir de l'aide sur le SUJET souhaité
-  **help(objet)** : affiche des informations sur l'objet Python
   spécifié (variable, fonction...) telles que son type, ses méthodes...
-  **help("module")** : affiche l'aide relative au module spécifié ; si
   le module est importé on peut directement faire **help(module)**
   (sans guillemets)
-  **help("module.objet")** : affiche directement l'aide relative à
   l'objet spécifié du module

Pour vous donner une idée taper :

.. code:: python

    help("print")
    print("Hello World !")
    1+1


Initiation à la syntaxe de Python
=================================

Le nom des variables en Python doit satisfaire plusieurs critères :

-  il ne doit pas commencer par un chiffre,
-  les minuscules et les majuscules sont distinguées,
-  il ne doit pas y avoir de caractères accentués.

Le dernier critère n'est plus à vérifier dans la version 3.x puisqu'il
travaille en Unicode.

Le langage Python est basé sur un certain nombre de documents proposant
des améliorations du langage: les PEPs.

Le guide des bonnes pratiques concernant l'écriture de code Python est
défini dans le PEP8.

Python est sensible aux noms des variables. Il n'aime pas les accents
(théoriquement plus avec Python 3 puisqu'il travaille en Unicode) mais
différencie les minuscules des majuscules. Nous resterons en ASCII pour
éviter tout litige.

Comme tout langage, Python a ses mots clés :

.. code:: python

    help("keywords")
    False       def         if          raise
    None        del         import      return
    True        elif        in          try
    and         else        is          while
    as          except      lambda      with
    assert      finally     nonlocal    yield
    break       for         not
    class       from        or
    continue    global      pass

Nous allons maintenant voir **les types simples** et **les types
containers**.

Les types simples
-----------------

4 types simples:

-  booléen,
-  entier,
-  réel,
-  complexe.

Les booléens
~~~~~~~~~~~~

.. code:: python

    # le type booléen prend les valeurs True ou False
    logique = True
    logique = False
    type(logique)

Les entiers
~~~~~~~~~~~

.. code:: python

    a = 2 ; i = -12
    # int : entier de précision illimitée !
    v = 2**80         # => 1208925819614629174706176
    # définition d'entiers en binaire, octal ou hexadéc.:
    k = 0b111         # => 7
    m = 0o77          # => 63
    n = 0xff          # => 255
    # conv. chaînes bin/octal/hexa en entier & vice-versa:
    int('111', 2)  # => 7,   et inverse: bin(7)   => '0b111'
    int('77', 8)   # => 63,  et inverse: oct(63)  => '0o77'
    int('ff', 16)  # => 255, et inverse: hex(255) => '0xff'

Quelques remarques

-  ; n'est utilisé que si il y a plusieurs instructions sur la même
   ligne.
-  La fonction **type()** vous permet de connaître le type de la
   variable.
-  Le symbole **#** permet d'écrire un commentaire jusqu'au retour à la
   ligne.

Les réels
~~~~~~~~~

Les réels sont codés en double précision (64 bits).

.. code:: python

    b = -3.3 ; r = 3e10
    abs(b)            # => 3.3
    # arrondi et conversion
    int(3.67)         # => 3 (int)
    int(-3.67)        # => -4 (int)
    round(3.67)       # => 4 (int), comme round(3.67, 0)
    round(3.67,1)     # => 3.7 (float)
    round(133,-1)     # => 130 (int)
    round(133,-2)     # => 100 (int)

Complexe flottant
~~~~~~~~~~~~~~~~~

.. code:: python

    cplx = 3.4 + 2.1j # ou:  cplx = complex(3.4, 2.1)
    cplx.real         # => 3.4
    cplx.imag         # => 2.1

**Remarque** : Python est un langage objet. Vous verrez qu'il existe des
méthodes associés à un type comme ici pour les nombres complexes.

Interaction avec l'écran et le clavier
--------------------------------------

Deux fonctions vont nous être utiles : **print** et **input**.

::

    - print(arguments)
    - input(prompt)

Attention, si vous souhaitez entrer une chaîne de caractères, il vous
faudra utiliser les guillemets (simples ou doubles).

.. code:: python

    help("print")
    help("input")

.. code:: python

    a = 3
    b = 2
    print('Le produit de', a, 'et', b, 'est', a*b)
    print('Le produit de %d et %e est %e'%(a, b, a*b))
    print('Le produit de {0} et {1:.3f} est {2:.3f}'.format(a, b, a*b))

.. code:: python

    nom = input("Quel est votre nom ?")

.. code:: python

    age = input("Quel âge avez-vous ?")

.. code:: python

    type(age)

.. code:: python

    age = int(age)
    print (bin(age))
    print (hex(age))

Instruction sur plusieurs lignes et commentaires
------------------------------------------------

Imaginons que nous sommes des linguistes ; on veut étudier la première
phrase de présentation du CNRS :

"Le Centre national de la recherche scientifique est un organisme public
de recherche (Établissement public à caractère scientifique et
technologique, placé sous la tutelle du Ministère de l'Éducation
nationale, de l'Enseignement supérieur et de la Recherche). Il produit
du savoir et met ce savoir au service de la société."

-  Faites afficher cette phrase : print("blabla")

.. code:: python

    print("Le Centre national de la recherche scientifique est un organisme public de recherche (Établissement public à caractère scientifique et technologique, placé sous la tutelle du Ministère de l'Éducation nationale, de l'Enseignement supérieur et de la Recherche).")

Pour des raisons de libilité et de portabilité, le `guide de style
Python <https://www.python.org/dev/peps/pep-0008/#maximum-line-length>`__
recommande de ne pas dépasser 79 caractères sur une ligne de code...

.. code:: python

    print("Le Centre national de la recherche scientifique est un organisme public de recherche "
          "(Établissement public à caractère scientifique et technologique, placé sous la "
          "tutelle du Ministère de l'Éducation nationale, de l'Enseignement supérieur et de la "
          "Recherche)")

Essayer en utilisant des apostrophes à la place des guillemets...

.. code:: python

    print('pourtant ca marche avec ces apostrophes')

Vous avez des apostrophes dans votre chaîne de caractères, utilisez les
guillemets :

.. code:: python

    print("Puisqu'il pleut, jusqu'à demain, c'est...")

Pour les commentaires sur plusieurs lignes, vous pouvez utiliser le
triple guillemet ou le triple apostrophe...

.. code:: python

    """
    ceci est un commentaire sur
    plusieurs lignes
    """
    '''
    ceci est un autre commentaire sur
    plusieurs lignes
    '''
    # Rappel : celui-ci n'est valable que jusqu'à la fin de la ligne


.. code:: python

    a = ( 3
         + 4)
    print(a)

Assignation simple et multiple
------------------------------

L'assignation se fait avec le signe **=**.

.. code:: python

    # Que valent a et b ?
    a = 2; b = 3
    a,b = 2,3
    print(a,b)
    a = b = 4
    print(a,b)
    a,b = b,a

On reviendra sur les tuples mais voici un exemple :

.. code:: python

    tuple1 = (1,'python')
    x,y = tuple1
    # Que valent x et y ?
    print(x, y)
    print(type(x))
    type(y)

Référencement
-------------

.. code:: python

    x = 'python'   # création de la donnée "python" référencé par "x"
    print (type(x))      # l'OBJET référencé est de type "str"
    print (id(x))        # adresse mémoire
    y = x          # nouvelle variable pointant sur la même donnée
    print (id(y)) # même adresse mémoire
    print (x)
    print (y)
    x = 123        # création de la donnée "123" et changement de référencement pour "x"
    print (type(x))      # l'OBJET référencé est de type "int"
    print (id(x))  # x pointe vers une autre adresse mémoire
    print (y)
    del(y)       # on supprime le référencement de y sur la donnée "python"
                 # le garbage collector supprimera la donnée automatiquement
                 # libérant ainsi de la mémoire...

Opérateurs de base
------------------

Les opérateurs mathématiques de base sont :

-  les classiques : \*\* + - / \* \*\*
-  la division entière tronquée : //
-  la puissance : \*\*
-  le modulo : %

.. code:: python

    print (3+2)
    print (3-2)
    print (3*2)

.. code:: python

    print (3/2)
    print (3/2.)
    print (3//2)

.. code:: python

    print (10**2)
    print (3%2)

On pourra noter que la fonction **divmod(a,b)** renvoie le tuple
(a//b,a%b).

.. code:: python

    divmod(7,3)

Les opérateurs **+** et \*\* \* \*\* s'appliquent aussi aux séquences
(chaînes, listes, tuples).

.. code:: python

    2*'a tchick ' + 3*'aie '

Les opérateurs pré- et post-incrémentation **++** et **--** n'existent
pas en Python. On utilisera à la place le **+=1** et le **-=1**.

.. code:: python

    x = 10                # x vaut 10
    x += 1                # x = x + 1 : x = 11
    x -= 1                # x = x - 1 : x = 10
    x *= 5                # x = x * 5 : x = 50
    x /= 10               # x = x / 10 : x = 5
    x %= 2                # x = x % 2 : 1
    print(x)

    fruit = 'pomme'     # renvoie "pomme"
    fruit +='s'          # renvoie "pommes"
    print(fruit)

    monTuple = (1,2,3)    # monTuple vaut (1,2,3)
    print (monTuple)
    monTuple += (4,)      # monTuple = monTuple + (4,) : monTuple = (1,2,3,4)
    print (monTuple)
    monTuple *= 2         # monTuple = monTuple * 2 : monTuple = (1,2,3,4,1,2,3,4)
    print (monTuple)

Opérateurs de comparaison, d'objets et logiques
-----------------------------------------------

Ces opérateurs renvoient les valeurs logiques **True** ou **False** qui
sont de type booléen.

-  Opérateurs de comparaison

+-------------+---------------------+
| opérateur                         |
+=============+=====================+
| **==**      | égal                |
+-------------+---------------------+
| **!=**      | différent           |
+-------------+---------------------+
| **<**       | inférieur           |
+-------------+---------------------+
| **<=**      | inférieur ou égal   |
+-------------+---------------------+
| **>**       | supérieur           |
+-------------+---------------------+
| **>=**      | supérieur ou égal   |
+-------------+---------------------+

-  Opérateurs d'objets

+-------------+-----------------------+
| opérateur                           |
+=============+=======================+
| is          | même objet            |
+-------------+-----------------------+
| in          | membre du container   |
+-------------+-----------------------+

-  Opérateurs logiques

+-------------+--------------------+
| opérateur                        |
+=============+====================+
| not         | négation logique   |
+-------------+--------------------+
| and         | ET logique         |
+-------------+--------------------+
| or          | OU logique         |
+-------------+--------------------+

.. code:: python

    a, b = 1, 2
    a < b

.. code:: python

    ["a",1,"b"] == [1,2,3]

.. code:: python

    a, b = [1,2], [1,2]
    a is b

.. code:: python

    a = b = [1,2]
    a is b

.. code:: python

    2 in [1,2,3]

En Python, on peut chaîner les comparaisons :

.. code:: python

    a = 12
    print(10 < a < 20)
    print(a > 10 and a < 20)


Les types de données (containers)
=================================

Contrairement aux types simples, les types containers permettent
d'instancier des objets contenant plusieurs données.

On distingue trois catégories de containers implémentés sous formes de 6
types de base :

-  les **séquences** : chaîne, liste, tuple
-  les **maps** ou **hashs** : dictionnaire
-  les **ensembles** : set, frozenset

Les séquences : chaînes de caractères
-------------------------------------

Les chaînes de caractères se délimitent par des apostrophes ou des
guillemets.

.. code:: python

    maChaine1 = "tout est bon dans le Python"
    maChaine2 = 'tout est bon dans le Python'

.. code:: python

    maChaine3 = 'C\'est comme ça'
    maChaine4 = "C'est comme ça" #pour la gestion des apostrophes dans les chaines
    maChaine4[4]

Les chaînes de caractères en Python sont **immutables**. Les chaînes ne
sont pas modifiables dans le sens où toutes modifications entraînent la
création de nouvelles chaînes. Le garbage collector effacera alors
automatique la ou les chaînes non référencées.

.. image:: _static/AccesAuxCaracteres.png

Une chaîne de caractères est une séquence ordonnée de caractères ce qui
permet de s'y référer par l'utilisation d'indices.

.. code:: python

    print ("bonjour"[0:3])
    print ("bonjour"[-4:])

-  concaténation

.. code:: python

    chain1 = "Tout est bon "
    chain2 = "Python"
    chain = chain1 + "dans le " + chain2
    print (chain)
    print ("2.0" + "re")
    3 * "po"

Il existe différentes méthodes associées aux chaînes comme par exemple :

-  **len(chain)** : renvoie la taille d'une chaîne,
-  **chain.find** : recherche une sous-chaîne dans la chaîne,
-  **chain.rstrip** : enlève les espaces de fin,
-  **chain.replace** : remplace une chaîne par une autre,
-  **chain.split** : découpe une chaîne,
-  **chain.isdigit** : renvoie True si la chaîne contient que des
   nombres, False sinon...
-  **...**

Pour en savoir plus, je vous invite à taper : **help(str)**

.. code:: python

    len(chain)

.. code:: python

    chain.split(" ")

Les séquences : les listes
--------------------------

.. image:: _static/Listes.png

Une liste dans Python permet de stocker une collection ordonnée sous
forme de séquence. La modification des éléments est dynamique et les
éléments peuvent être hétérogènes (de n'importe quel type).


.. code:: python

    maListe = [1, 2.3, "ab", 3.4 + 2.1j, [0, 1, 2]]
    maListe




.. parsed-literal::

    [1, 2.3, 'ab', (3.4+2.1j), [0, 1, 2]]



.. code:: python

    maListe[4][1]




.. parsed-literal::

    1



.. code:: python

    print(type(maListe))
    print(type(maListe[3]))


.. parsed-literal::

    <class 'list'>
    <class 'complex'>


-  Modification

.. code:: python

    Lis = maListe
    print (Lis)
    maListe[2] = 6
    maListe[4][1] = 3
    print (maListe)
    print (Lis)

-  Concaténation

.. code:: python

    l = [0] * 10
    l

-  Copie de listes

   Si l'on souhaite dupliquer les données d'une liste, il faut
   comprendre qu'en Python, une simple assignation
   **maListeBis=maListe** n'effectue pas une copie des données. Les
   variables **maListeBis** et **maListe** référencent la même liste !

   Pour recopier correctement une liste :

   -  **maListeBis**\ =\ **maListe[:]** duplique seulement les éléments
      de premier niveau. Les listes et/ou dictionnaires imbriqués
      continueront en revanche d'être référencés et non dupliqués.

   -  **maListeBis**\ =copy.deepcopy(\ **maListe**) permet une copie
      complète récursive. Il faudra néanmoins importer le module
      **copy**.

Plusieurs méthodes peuvent être appliquées aux listes.

.. code:: python

    maListe = [1,2.3,"ab",3.4 + 2.1j,[0,1,2]]
    # Que permettent ces méthodes :
    len(maListe)                  # longueur de la liste
    maListe.sort()                  # trie la liste (ici, ce n'est pas possible: les éléments ne sont pas comparables)
    maListe.append(5)             # ajoute un élement à la fin
    maListe.insert(0,'a')         # ajoute l'élément 'a' en position 0 dans la liste
    maListe.extend(['a','b'])     # ajoute les éléments 'a' et 'b' à la liste (concaténation de listes)
    maListe.reverse()               # inverse l'ordre des éléments
    maListe.index(5)              # première position à laquelle apparaît la valeur 5 dans la liste
    maListe.remove(5)             # supprime la première occurrence de la valeur 5 dans la liste
    maListe.pop()                   # supprime et renvoie le dernier élément de la liste

Pour plus d'aide, je vous invite à regarder l'aide : **help(list)**

Les séquences : les tuples
--------------------------

Le tuple Python est l'implémentation de la liste mais en lecture seule.
Le tuple Python est donc une collection ordonnée non modifiable
d'éléments hétérogènes. On retrouvera ainsi les mêmes méthodes que pour
les listes sans celles concernant les modifications.

-  L'intérêt des tuples par rapport aux listes est qu'ils occupent moins
   d'espace mémoire et que leur traitement est plus efficace.

.. code:: python

    monTuple = 'a','b','c','d'
    monTuple[0]

.. code:: python

    monTuple[0] = 'alpha'

.. code:: python

    monTuple = ('alpha',) + monTuple[1:]
    monTuple

Si le premier niveau des tuples n'est pas modifiable, on peut en
revanche modifier les objets modifiables imbriqués.

.. code:: python

    monTuple2 = (1, 2, [3, 4], 6)
    monTuple2[2][0] = 1
    monTuple2

On peut convertir un tuple en liste avec la fonction **list()**

.. code:: python

    print (monTuple2)
    liste2 = list(monTuple2)
    print (liste2)
    type(liste2)

On peut également concaténer deux tuples :

.. code:: python

    monTuple + monTuple2

Pour plus d'aide, je vous invite à regarder l'aide : **help(tuple)**

Les maps ou hash : dictionnaire
-------------------------------

Un dictionnaire est une liste modifiable d'éléments hétérogènes mais
indicés par des clés.

-  Un dictionnaire n'est pas une séquence.
-  Un dictionnaire est constitué de clés et de valeurs.
-  On ne peut pas concaténer un dictionnaire avec un autre.

.. code:: python

    dic = {'a': 1, 'b': 2}
    dic

.. code:: python

    dic2 = {}
    dic2['a'] = 1
    dic2['b'] = 2
    dic2

.. code:: python

    dic3 = dict(a=1, b=2)
    dic3

.. code:: python

    dic4 = dict(zip(('a','b'), (1,2)))
    dic4

-  Pour récupérer une valeur :

.. code:: python

    dic['a']
    dic.get('a')

-  Ajouter un élément, modifier une valeur, supprimer un élément :

.. code:: python

    dic[1] = 7       # Ajoute un élément (clé, valeur)
    dic['b'] = 3     # On modifie la valeur "2" par "3"
    del(dic['b'])    # On supprime l'élément dont la clé est 'b'

    # A vous de découvrir ce qui se passe pour ceci :
    val = dic.pop('b')

-  Ajouter plusieurs éléments, fusion de dictionnaire :

.. code:: python

    dicBis = {'c':3,'d':4}
    dic.update(dicBis)
    dic

-  Pour copier un dictionnaire, utiliser un nouveau référencement ne
   suffit pas, il faut utiliser la méthode **copy**.

.. code:: python

    # copier le dictionnaire dic dans le dictionnaire copieDeDic
    # quelle fonction vous permet de vous assurer de la bonne copie ?

Plusieurs méthodes existent pour les dictionnaires :

.. code:: python

    len(dic)      #taille du dictionnaire
    dic.keys()      #renvoie les clés du dictionnaire sous forme de liste
    dic.values()    #renvoie les valeurs du dictionnaire sous forme de liste
    dic.has_key(cle)   #renvoie True si la clé existe, False sinon
    dic.get(cle)       #donne la valeur de la clé si elle existe

Remarque : Les méthodes **.keys()**, **.values()** et **.items()**
retournent des objets permettant d'itérer, par une boucle **for**,
respectivement les clés, les valeurs et les paires clés/valeurs.

Pour plus d'aide : **help(dic)**

Les ensembles : set
-------------------

Ce type permet de créer des **collections de données non ordonnées
modifiables** constituées d'\ **éléments uniques** de type
**immutable**.

Le type set n'est donc pas une séquence ; en revanche, il supporte
l'itération. C'est comme un dictionnaire avec des clés sans valeurs.

.. code:: python

    monSet = {1,2,3,1,4}
    monSet # éléments uniques

.. code:: python

    monSet1 = set((1,2,1,4,3))
    monSet1

Regardez ce que renvoie la commande **type**. De quelle longueur est ce
set ?

-  Ajout d'éléments

.. code:: python

    monSet = {1,2,3,1,4}
    monSet

.. code:: python

    monSet |= {5,6}
    monSet

.. code:: python

    monSet = monSet|{7,8}
    monSet

.. code:: python

    monSet.update({9,10})
    monSet

.. code:: python

    monSet.add(11)
    monSet

-  D'autres méthodes

.. code:: python

    monSet.remove(3)  # détruit l'élément "3"
    monSet.discard(3) # détruit l'élément "3" sans générer d'erreur si l'élément est absent
    monSet.pop()      # retourne un élément arbitraire et le détruit
    monSet.clear()    # détruit tous les éléments

-  Opérations propres aux ensembles

.. code:: python

    monSet | monSet1  # union        => {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}
    monSet & monSet1  # intersection => {1, 2, 3, 4}
    monSet - monSet1  # différence   => {5, 6, 7, 8, 9, 10, 11}
    monSet1 - monSet  # différence   => set()
    monSet ^ monSet1  # ou exclusif  => {5, 6, 7, 8, 9, 10, 11}

-  Tests

.. code:: python

    {'a','b'} == {'b','a'}         # sets identiques => True
    'a' in {'a','b','c'}           # présence d'un élément isolé => True
    {'a','b'} <= {'a','b','c','d'} # présence de tous les éléments => True

-  Copie d'un set

   Comme pour les listes et les dictionnaires, on en peut pas copier en
   faisant un simple tonSet=monSet. Il faut utiliser :

.. code:: python

    tonSet = monSet.copy()

Pour plus d'aide : **help(set)**

Les ensembles : frozenset
-------------------------

Un type frozenset est un **set** mais **immutable** (non-modifiable).
Tout ce qui s'applique aux sets, s'appliquent également au frozenset à
l'exception de tout ce qui concerne les modifications.

.. code:: python

    monFrozenset = frozenset({'x','y','z','x'}) # ou bien
    monFrozenset = frozenset(('x','y','z','x'))

Pour plus d'aide : **help(frozenset)**

Récapitulatif sur les différents types de données
-------------------------------------------------

-  Simple

   -  **modifiable**

      -  aucun

   -  **immutable**

      -  booléen
      -  entier
      -  entier flottant
      -  complexe

-  Container

   -  **modifiable**

      -  liste (séquence)
      -  set (ensemble)
      -  dictionnaire (map)

   -  **immutable**

      -  tuple (séquence)
      -  frozenset (ensemble)
      -  chaîne (séquence)


Les structures de contrôle
==========================

En Python, les structures de contrôle existent par l'indentation des
blocs de code. L'indentation est donc très importante en Python. Cette
indentation apporte au langage une lisibilité et légerté au code
(absence d'accolades, points-virgules,...)

.. image:: _static/Instructions_Blocs.png

Le `guide de style
Python <https://www.python.org/dev/peps/pep-0008/#indentation>`__
mentionne qu'il est préférable d'utiliser les espaces plutôt que la
tabulation. Il indique même 4 espaces par niveau d'indentation.

Exécution conditionnelle IF - ELIF - ELSE
------------------------------------------

La forme de cette structure de contrôle est montrée ci-dessous. Les
parties **elif** et **else** sont bien sûr facultatives.

.. code:: python

    a = 10.
    if a > 0:
        print ("a est strictement positif")
        if a >= 10:
            print ("a est un nombre")
        else:
            print ("a est un chiffre")
        a += 1
    elif a is not 0:
        print ("a est strictement negatif")
    else:
        print ("a est nul")
    print (a)

Comme dans d'autres langages, Python offre l'écriture d'expressions
conditionnelles :

.. code:: python

    a = 10
    print ("a est positif" if a>=0 else "a est négatif")

.. code:: python

    if a >= 0:
        print ("a est positif")
    else:
        print ("a est négatif")

Boucle WHILE
------------

La boucle ``while`` permet d'exécuter un bloc d'instruction aussi
longtemps qu'une condition (expression logique) est vraie.

.. code:: python

    while <test1>:
        <blocs d’instructions 1>
        if <test2>:
            break
        if <test3>:
            continue
    else:
        <blocs d’instructions 2>

-  **break** : sort de la boucle sans passer par else
-  **continue** : remonte au début de la boucle
-  **pass** : ne fait rien
-  **else** : lancé si et seulement si la boucle se termine normalement

.. code:: python

    nb = 1 ; stop = 3
    # Affiche le carré des nombres de nb à stop
    while nb <= stop:
        print(nb, nb**2)
        nb += 1

Boucle FOR
----------

La boucle **for** permet d'itérer les valeurs d'une liste, d'un tuple,
d'une chaîne ou de tout objet itérable. Comme dans les autres structures
de contrôle, le caractère : (double point) définit le début du bloc
d'instruction contrôlé par **for**.

.. code:: python

    for <cible> in <objet>:
        <blocs d’instructions>
        if <test1>:
            break
        if <test2>:
            continue
    else:
        <blocs d’instructions>

.. code:: python

    sum = 0
    for i in [1, 2, 3, 4]:
        sum += i
    sum

.. code:: python

    L = [ x + 10 for x in range(10)]
    L

-  Sur une liste ou un tuple :

.. code:: python

    maList = [1,2,3]
    for n in maList:
        print (n)

Pour itérer sur une suite de nombres entiers, on utilise souvent la
fonction **range** :

.. code:: python

    for index in range(len(maList)):
        print(index, maList[index])

Vous pouvez également utiliser la fonction **enumerate** qui retourne un
objet grâce auquel vous pouvez itérer sur les indices et les valeurs
d'une séquence.

.. code:: python

    for index,val in enumerate(maList):
        print(index, val)

-  Sur une chaîne :

.. code:: python

    voyelles = 'aeiouy'
    for car in 'chaine de caracteres':
        if car in voyelles:
            print (car)

-  Sur un dictionnaire :

.. code:: python

    carres = {}
    for n in range(1,4):
        carres[n] = n**2
    print (carres)

.. code:: python

    for k in carres:  # itère par défaut sur la clé !
      # identique à: for k in carres.keys():
        print (k)

MAP et ZIP
----------

-  **map** applique une méthode sur une ou plusieurs séquences.
-  **zip** permet de parcourir plusieurs séquences en parallèle.

Remarque : **map** peut être beaucoup plus rapide que l'utilisation de
**for**.

.. code:: python

    S = '0123456789'
    M = map(int, S)
    for x in M:
        print(x)

.. code:: python

    L1 = [1, 2, 3]
    L2 = [4, 5, 6]
    for (x, y) in zip(L1, L2):
        print (x, y, '--', x + y)

Exercices
=========

Exercice 1
----------

Je vous propose de progresser vers l'utilisation de l'éditeur de
fichier. Nous allons donc utiliser la fenêtre de gauche.

Écrivez un programme qui convertisse en degrés Celsius une température
exprimée au départ en degrés Fahrenheit, ou l’inverse.

-  La formule de conversion est : TF = TC ×1,8+32
-  TF : température en degré Fahrenheit
-  TC : température en degré Celsius

Solution
~~~~~~~~

Une solution non optimale mais du niveau des notions vues dans cette
première partie du cours...

.. code:: python

    """
    Created on Wed Nov 26 22:31:38 2014

    Écrivez un programme qui convertisse en degrés Celsius une température 
    exprimée au départ en degrés Fahrenheit, ou l’inverse.

    La formule de conversion est : TF = TC × 1,8 + 32
    TF : température en degré Fahrenheit
    TC : température en degré Celsius

    @author: chalgand
    """
    goodAnswer = False
    while goodAnswer == False:
        print("Dans quel sens voulez-vous faire la conversion ?")
        print("1 - Degré Celsius  vers  Degré Fahrenheit")
        print("2 - Degré Fahrenheit vers  Degré Celsius")
        answer = input("Choisissez le sens : ")
        if answer == "1" or answer == "2":
            goodAnswer = True
            print(answer)

    if answer == "1":
        text = "Degré Celsius vers Degré Fahrenheit" 
    else:
        text = "Degré Fahrenheit vers  Degré Celsius"
    print("Vous avez choisi la conversion : " + text)

    temp = "une mauvaise température"
    while type(temp) != int:
        temp = int(input("Veuillez entrer la température à convertir: "))

    if answer == "1":
        print(temp, " degré(s) Celsius = ", temp*1.8+32, 
              " degré(s) Fahrenheit")
    else:
        print(temp, " degré(s) Fahrenheit = ", (temp-32)/1.8, 
              " degré(s) Celsius")

Exercice 2
----------

Écrivez un code Python qui affiche une suite de 12 nombres dont chaque
terme est égal au triple du précédent.

Exercice 3
----------

Écrivez un code Python qui affiche

::

    *
    **
    ***
    ****
    *****

Exercice 4
----------

Que fait le code suivant ?

.. code:: python

    ch = 'une chaine'
    i = ch[4]
    print(i)
    j = ch(4)
    print(j)

Exercice 5
-----------

Soient les listes suivantes :

.. code:: python

    t1 = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    t2 = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin',
          'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre']

Écrivez un code Python qui insère dans la seconde liste tous les
éléments de la première, de telle sorte que chaque nom de mois soit
suivi du nombre de jours correspondant :

::

    ['Janvier',31,'Février',28,'Mars',31, etc...].

Exercice 6
----------

Soit le dictionnaire suivant :

.. code:: python

    dico1 = {'computer': 'ordinateur', 'mouse': 'souris',
             'printer': 'imprimante', 'keyboard': 'clavier'}

Écrivez un code Python qui inverse les clés et les valeurs permettant de
passer d'un dictionnaire anglais/français à un dictionnaire
français/anglais.

Exercice 7
-----------

Soit la chaîne de caractères suivante :

.. code:: python

    chaine = 'Ceci est une chaine.'

-  Est-ce que ``chaine`` est un objet? Quelles sont ses méthodes?
-  Mettez dans une variable ``CHAINE`` cette chaîne en majuscules.
-  Mettez dans une variable ``nb_mot`` le nombre de mots de la phrase.
-  Comptez le nombre d'occurrences de chaque lettre (on utilisera un
   dictionnaire et la fonction **get**).
